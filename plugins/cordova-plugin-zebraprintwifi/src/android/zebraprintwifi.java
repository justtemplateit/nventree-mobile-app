
/***********************************************
 * CONFIDENTIAL AND PROPRIETARY 
 * 
 * The source code and other information contained herein is the confidential and the exclusive property of
 * ZIH Corp. and is subject to the terms and conditions in your end user license agreement.
 * This source code, and any other information contained herein, shall not be copied, reproduced, published, 
 * displayed or distributed, in whole or in part, in any medium, by any means, for any purpose except as
 * expressly permitted under such license agreement.
 * 
 * Copyright ZIH Corp. 2012
 * 
 * ALL RIGHTS RESERVED
 ***********************************************/
/*
package com.zebra.android.devdemo.connectivity;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.zebra.android.devdemo.R;
import com.zebra.android.devdemo.util.DemoSleeper;
import com.zebra.android.devdemo.util.SettingsHelper;
import com.zebra.sdk.comm.BluetoothConnection;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.comm.TcpConnection;
import com.zebra.sdk.printer.PrinterLanguage;
import com.zebra.sdk.printer.ZebraPrinter;
import com.zebra.sdk.printer.ZebraPrinterFactory;
import com.zebra.sdk.printer.ZebraPrinterLanguageUnknownException;

public class ConnectivityDemo extends Activity {

    private Connection printerConnection;
    private RadioButton btRadioButton;
    private ZebraPrinter printer;
    private TextView statusField;
    private EditText macAddress, ipDNSAddress, portNumber;
    private Button testButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.connection_screen_with_status);

        ipDNSAddress = (EditText) this.findViewById(R.id.ipAddressInput);
        ipDNSAddress.setText(SettingsHelper.getIp(this));

        portNumber = (EditText) this.findViewById(R.id.portInput);
        portNumber.setText(SettingsHelper.getPort(this));

        macAddress = (EditText) this.findViewById(R.id.macInput);
        macAddress.setText(SettingsHelper.getBluetoothAddress(this));

        statusField = (TextView) this.findViewById(R.id.statusText);
        btRadioButton = (RadioButton) this.findViewById(R.id.bluetoothRadio);

        testButton = (Button) this.findViewById(R.id.testButton);
        testButton.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                new Thread(new Runnable() {
                    public void run() {
                        enableTestButton(false);
                        Looper.prepare();
                        doConnectionTest();
                        Looper.loop();
                        Looper.myLooper().quit();
                    }
                }).start();
            }
        });

        RadioGroup radioGroup = (RadioGroup) this.findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.bluetoothRadio) {
                    toggleEditField(macAddress, true);
                    toggleEditField(portNumber, false);
                    toggleEditField(ipDNSAddress, false);
                } else {
                    toggleEditField(portNumber, true);
                    toggleEditField(ipDNSAddress, true);
                    toggleEditField(macAddress, false);
                }
            }
        });
    }

    private void toggleEditField(EditText editText, boolean set) {
        // Note: Disabled EditText fields may still get focus by some other means, and allow text input.
        // See http://code.google.com/p/android/issues/detail?id=2771
        editText.setEnabled(set);
        editText.setFocusable(set);
        editText.setFocusableInTouchMode(set);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (printerConnection != null && printerConnection.isConnected()) {
            disconnect();
        }
    }

    private void enableTestButton(final boolean enabled) {
        runOnUiThread(new Runnable() {
            public void run() {
                testButton.setEnabled(enabled);
            }
        });
    }

    private boolean isBluetoothSelected() {
        return btRadioButton.isChecked();
    }

    public ZebraPrinter connect() {
        setStatus("Connecting...", Color.YELLOW);
        printerConnection = null;
        if (isBluetoothSelected()) {
            printerConnection = new BluetoothConnection(getMacAddressFieldText());
            SettingsHelper.saveBluetoothAddress(this, getMacAddressFieldText());
        } else {
            try {
                int port = Integer.parseInt(getTcpPortNumber());
                printerConnection = new TcpConnection(getTcpAddress(), port);
                SettingsHelper.saveIp(this, getTcpAddress());
                SettingsHelper.savePort(this, getTcpPortNumber());
            } catch (NumberFormatException e) {
                setStatus("Port Number Is Invalid", Color.RED);
                return null;
            }
        }

        try {
            printerConnection.open();
            setStatus("Connected", Color.GREEN);
        } catch (ConnectionException e) {
            setStatus("Comm Error! Disconnecting", Color.RED);
            DemoSleeper.sleep(1000);
            disconnect();
        }

        ZebraPrinter printer = null;

        if (printerConnection.isConnected()) {
            try {
                printer = ZebraPrinterFactory.getInstance(printerConnection);
                setStatus("Determining Printer Language", Color.YELLOW);
                PrinterLanguage pl = printer.getPrinterControlLanguage();
                setStatus("Printer Language " + pl, Color.BLUE);
            } catch (ConnectionException e) {
                setStatus("Unknown Printer Language", Color.RED);
                printer = null;
                DemoSleeper.sleep(1000);
                disconnect();
            } catch (ZebraPrinterLanguageUnknownException e) {
                setStatus("Unknown Printer Language", Color.RED);
                printer = null;
                DemoSleeper.sleep(1000);
                disconnect();
            }
        }

        return printer;
    }

    public void disconnect() {
        try {
            setStatus("Disconnecting", Color.RED);
            if (printerConnection != null) {
                printerConnection.close();
            }
            setStatus("Not Connected", Color.RED);
        } catch (ConnectionException e) {
            setStatus("COMM Error! Disconnected", Color.RED);
        } finally {
            enableTestButton(true);
        }
    }

    private void setStatus(final String statusMessage, final int color) {
        runOnUiThread(new Runnable() {
            public void run() {
                statusField.setBackgroundColor(color);
                statusField.setText(statusMessage);
            }
        });
        DemoSleeper.sleep(1000);
    }

    private String getMacAddressFieldText() {
        return macAddress.getText().toString();
    }

    private String getTcpAddress() {
        return ipDNSAddress.getText().toString();
    }

    private String getTcpPortNumber() {
        return portNumber.getText().toString();
    }

    private void doConnectionTest() {
        printer = connect();
        if (printer != null) {
            sendTestLabel();
        } else {
            disconnect();
        }
    }

    private void sendTestLabel() {
        try {
            byte[] configLabel = getConfigLabel();
            printerConnection.write(configLabel);
            setStatus("Sending Data", Color.BLUE);
            DemoSleeper.sleep(1500);
            if (printerConnection instanceof BluetoothConnection) {
                String friendlyName = ((BluetoothConnection) printerConnection).getFriendlyName();
                setStatus(friendlyName, Color.MAGENTA);
                DemoSleeper.sleep(500);
            }
        } catch (ConnectionException e) {
            setStatus(e.getMessage(), Color.RED);
        } finally {
            disconnect();
        }
    }

    // Returns the command for a test label depending on the printer control language
    // The test label is a box with the word "TEST" inside of it
    private byte[] getConfigLabel() {
        PrinterLanguage printerLanguage = printer.getPrinterControlLanguage();

        byte[] configLabel = null;
        if (printerLanguage == PrinterLanguage.ZPL) {
            configLabel = "^XA^FO17,16^GB379,371,8^FS^FT65,255^A0N,135,134^FDTEST^FS^XZ".getBytes();
        } else if (printerLanguage == PrinterLanguage.CPCL) {
            String cpclConfigLabel = "! 0 200 200 406 1\r\n" + "ON-FEED IGNORE\r\n" + "BOX 20 20 380 380 8\r\n" + "T 0 6 137 177 TEST\r\n" + "PRINT\r\n";
            configLabel = cpclConfigLabel.getBytes();
        }
        return configLabel;
    }

}
*/




















// based on 
// https://www.outsystems.com/blog/posts/how-to-create-a-cordova-plugin-from-scratch/

package com.justapplications.cordova.plugin;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

// The native Toast API
import android.widget.Toast;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
//import android.os.Looper;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;

// Cordova-required packages
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CordovaInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

//import com.zebra.android.devdemo.R;
//import com.zebra.android.devdemo.util.DemoSleeper;
//import com.zebra.android.devdemo.util.SettingsHelper;
//import com.zebra.sdk.comm.BluetoothConnection;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.comm.TcpConnection;
import com.zebra.sdk.printer.PrinterLanguage;

import com.zebra.sdk.device.ZebraIllegalArgumentException;
import com.zebra.sdk.graphics.internal.ZebraImageAndroid;

import com.zebra.sdk.printer.PrinterStatus;
import com.zebra.sdk.printer.PrinterStatusMessages;

import com.zebra.sdk.printer.ZebraPrinter;
import com.zebra.sdk.printer.ZebraPrinterFactory;
import com.zebra.sdk.printer.ZebraPrinterLanguageUnknownException;
import com.zebra.sdk.printer.ZebraPrinterLinkOs;

public class zebraprintwifi extends CordovaPlugin {

    //final String DEFAULT_IP_ADDRESS = "192.168.128.212";
    //private static final String DURATION_LONG = "long";

    //private static final String LOG_TAG = "Printer";
    //private LabelPrinter printer;

    private Connection printerConnection;
   
    private ZebraPrinter printer;
    //private TextView statusField;
    //private EditText macAddress, ipDNSAddress, portNumber;

    private static File file = null;

    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        // Create an instance( LabelPrinter class )

        //printer = new LabelPrinter();
    }

    public void disconnect(int portType, String address) {
        try {
            //setStatus("Disconnecting", Color.RED);
            Toast.makeText(cordova.getActivity(), "Disconnecting from "+address, Toast.LENGTH_SHORT).show();
            if (printerConnection != null) {
                printerConnection.close();
            }
            //setStatus("Not Connected", Color.RED);
            Toast.makeText(cordova.getActivity(), "Not Connected", Toast.LENGTH_SHORT).show();
        } catch (ConnectionException e) {
            //setStatus("COMM Error! Disconnected", Color.RED);
            Toast.makeText(cordova.getActivity(), "COMM Error! Disconnected", Toast.LENGTH_SHORT).show();
        } finally {
            //enableTestButton(true);
            // dupa conectare pot rula ceva gen activarea unui buton sau print
        }
    }

    public ZebraPrinter connect(int portType, String address) {
        //setStatus("Connecting...", Color.YELLOW);
        Toast.makeText(cordova.getActivity(), "Connecting...", Toast.LENGTH_SHORT).show();
        printerConnection = null;
        
        try {
            //int port = Integer.parseInt(portType);
            printerConnection = new TcpConnection(address, portType);
            //SettingsHelper.saveIp(this, address);
            //SettingsHelper.savePort(this, port);
        } catch (NumberFormatException e) {
            //setStatus("Port Number Is Invalid", Color.RED);
            Toast.makeText(cordova.getActivity(), "Port Number Is Invalid", Toast.LENGTH_SHORT).show();
            return null;
        }
        
        try {
            printerConnection.open();
            //setStatus("Connected", Color.GREEN);
            Toast.makeText(cordova.getActivity(), "Connected", Toast.LENGTH_LONG).show();
        } catch (ConnectionException e) {
            //setStatus("Comm Error! Disconnecting", Color.RED);
            Toast.makeText(cordova.getActivity(), "Comm Error! Disconnecting", Toast.LENGTH_SHORT).show();
            //DemoSleeper.sleep(1000);
            disconnect(portType, address);
        }

        ZebraPrinter printer = null;

        if (printerConnection.isConnected()) {
            try {
                printer = ZebraPrinterFactory.getInstance(printerConnection);
                //setStatus("Determining Printer Language", Color.YELLOW);
                Toast.makeText(cordova.getActivity(), "Determining Printer Language", Toast.LENGTH_SHORT).show();
                PrinterLanguage pl = printer.getPrinterControlLanguage();
                //setStatus("Printer Language " + pl, Color.BLUE);
                Toast.makeText(cordova.getActivity(), "Printer Language " + pl, Toast.LENGTH_SHORT).show();
            } catch (ConnectionException e) {
                //setStatus("Unknown Printer Language", Color.RED);
                Toast.makeText(cordova.getActivity(), "Unknown Printer Language", Toast.LENGTH_SHORT).show();
                printer = null;
                //DemoSleeper.sleep(1000);
                disconnect(portType, address);
            } catch (ZebraPrinterLanguageUnknownException e) {
                //setStatus("Unknown Printer Language", Color.RED);
                Toast.makeText(cordova.getActivity(), "Unknown Printer Language", Toast.LENGTH_SHORT).show();
                printer = null;
                //DemoSleeper.sleep(1000);
                disconnect(portType, address);
            }
        }

        return printer;
    }

    private void printPhotoFromExternal(final Bitmap bitmap, int portType, String address) {
        new Thread(new Runnable() {
            public void run() {
                try {
                    //getAndSaveSettings();

                    //Looper.prepare();
                    //helper.showLoadingDialog("Sending image to printer");
                    Toast.makeText(cordova.getActivity(), "Sending image to printer", Toast.LENGTH_SHORT).show();
                    Connection connection = new TcpConnection(address, portType);
                    connection.open();
                    ZebraPrinter printer = ZebraPrinterFactory.getInstance(connection);
                    
//PDF PRINT
// https://github.com/Zebra/LinkOS-Android-Samples/tree/PDFPrint


                    //if (((CheckBox) findViewById(R.id.checkBox)).isChecked()) {
                        //printer.storeImage(printStoragePath.getText().toString(), new ZebraImageAndroid(bitmap), 550, 412);
                    //} else {
                        printer.printImage(new ZebraImageAndroid(bitmap), 0, 0, 550, 412, false);
                    //}

                    connection.close();

                    if (file != null) {
                        file.delete();
                        file = null;
                    }



        			file = new File(Environment.getExternalStorageDirectory(), "tempPic.jpg");








                } catch (ConnectionException e) {
                    //helper.showErrorDialogOnGuiThread(e.getMessage());
                    Toast.makeText(cordova.getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (ZebraPrinterLanguageUnknownException e) {
                    //helper.showErrorDialogOnGuiThread(e.getMessage());
                    Toast.makeText(cordova.getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (ZebraIllegalArgumentException e) {
                    //helper.showErrorDialogOnGuiThread(e.getMessage());
                    Toast.makeText(cordova.getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } finally {
                    bitmap.recycle();
                    //helper.dismissLoadingDialog();
                    //Looper.myLooper().quit();
                }
            }
        }).start();

    }

    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) {
        // Verify that the user sent a correct action
        String message;
        String address;
        try {
            JSONObject options = args.getJSONObject(0);
            message = options.getString("message");
            address = options.getString("address");
        } catch (JSONException e) {
            callbackContext.error("Error encountered: " + e.getMessage());
            return false;
        }
        if(action.equals("connect")) {
            int port = Integer.parseInt(message);
            connect(port, address);
            //status(printer);
        } else if(action.equals("print")) {
            // Create an instance( LabelDesign class )
            //LabelDesign design = new LabelDesign();

          	int port = Integer.parseInt(message);


            Uri imgPath = data.getData();
            Bitmap myBitmap = null;

                try {
                    myBitmap = Media.getBitmap(getContentResolver(), imgPath);
                } catch (FileNotFoundException e) {
                    //helper.showErrorDialog(e.getMessage());
                    Toast.makeText(cordova.getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    //helper.showErrorDialog(e.getMessage());
                    Toast.makeText(cordova.getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                printPhotoFromExternal(myBitmap, port, address);
            









        } else if(action.equals("disconnect")){
            int port = Integer.parseInt(message);
            disconnect(port, address);
        } else if(action.equals("status")){
            //status();
        } else {
            callbackContext.error("\"" + action + "\" is not a recognized action.");
            return false;
        }

        // Send a positive result to the callbackContext
        PluginResult pluginResult = new PluginResult(PluginResult.Status.OK);
        callbackContext.sendPluginResult(pluginResult);
        return true;
    }
}
routes = [
    {
        path: '/',
        url: './index.html',
        on: {
            pageInit: function(page) {
                function intvrefresh(minutes){
                    clearInterval(refresh);
                    refresh = setTimeout(function() {
                        var curPage = app.views.main.router.currentPageEl.dataset.name;
                        if(curPage == 'home' || curPage == 'lockscreen' || curPage == 'passcode' || curPage == 'changepin' || curPage == 'offline' || curPage == 'pin'){
                            return;
                        }
                        toast('Device locked due to inactivity.');
                        app.popup.close();
                        //app.dialog.close();
                        app.views.main.router.navigate('/lockscreen/');
                    }, minutes * 60 * 1000);
                }
                if(localStorage.getItem('defaultTimeout') == null){
                    localStorage.setItem('defaultTimeout', defaultTimeout);
                }
                intvrefresh(localStorage.getItem('defaultTimeout'));
                $(document).on('keypress click', function() {
                    intvrefresh(localStorage.getItem('defaultTimeout'));
                });

                /*
                var numpadInline = app.keypad.create({
                    inputEl: '#demo-numpad-inline',
                    containerEl: '#numpad-inline-container',
                    toolbar: false,
                    valueMaxLength: 4,
                    dotButton: false,
                    formatValue: function (value) {
                        value = value.toString();
                        return ('****').substring(0, value.length) + ('____').substring(0, 4 - value.length);
                    },
                    on: {
                        change(keypad, value) {
                            value = value.toString();
                            var existing = localStorage.getItem('users');
                                existing = existing ? JSON.parse(existing) : {};
                            if (value.length === 4) {
                                signinbyPIN(value, function(data){
                                    console.log(data);
                                });
                                
                                setUserPIN(value, function(e){
                                    if(e === 'OK') {
                                        for (var i in existing) {
                                            if (existing[i].UserId == localStorage.getItem('UserId')) {
                                                existing[i].PinCode = value;
                                                break;
                                            }
                                        }
                                        localStorage.setItem('users', JSON.stringify(existing));
                                        localStorage.setItem('PinCode', true);
                                        page.detail.router.navigate('/dashboard/');
                                    } else {
                                        numpadInline.setValue('');
                                    }
                                });
                                
                            }
                        }
                    }
                });
                */
            }
        }
    },{
        path: '/offline/',
        url: './pages/offline.html',
        on: {
            pageInit: function() {
                app.dialog.preloader('Please wait for connection or turn on the network...');
            }
        }
    },{
        path: '/lockscreen/',
        url: './pages/lockscreen.html',
    },{
        path: '/passcode/',
        url: './pages/passcode.html',
        /*
        statusbar:{
            iosBackgroundColor:'#ffffff',
            iosTextColor:'#000000',
        },
        */
        on: {
            pageInit: function (page) {
                // https://github.com/framework7io/framework7-plugin-keypad#usage
                var numpadInline = app.keypad.create({
                    inputEl: '#demo-numpad-inline',
                    containerEl: '#numpad-inline-container',
                    toolbar: false,
                    valueMaxLength: 4,
                    dotButton: false,
                    formatValue: function (value) {
                        value = value.toString();
                        //console.log(value)
                        //console.log(('****').substring(0, value.length) + ('____').substring(0, 4 - value.length))
                        return ('****').substring(0, value.length) + ('____').substring(0, 4 - value.length);
                    },
                    on: {
                        change(keypad, value) {
                            value = value.toString();
                            var cId = localStorage.getItem('ContextID');
                            if (value.length === 4) {
                                signinbyPIN(cId, value, function(data){
                                    console.log(data);
                                    if(data.Success){
                                        localStorage.setItem('ContextID', data.Contexts[0].ID);
                                        localStorage.setItem('StoreName', data.Contexts[0].Name);
                                        //localStorage.setItem('username', existing[i].username);
                                        localStorage.setItem('UserId', data.UserId);
                                        localStorage.setItem('PinCode', value);
                                        localStorage.setItem('FirstName', data.FirstName);
                                        localStorage.setItem('LastName', data.LastName);
                                        localStorage.setItem('Token', data.Token.replace('nvt.api.',''));
                                        page.detail.router.navigate('/dashboard/');
                                        getConfig();
                                        return;
                                    }
                                    /*
                                    app.dialog.alert('Wrong passcode! Please try again.', function () {
                                        app.views.main.router.navigate(app.views.main.router.currentRoute.url, {
                                            ignoreCache: true,
                                            reloadCurrent: true
                                        });
                                    });
                                    */
                                });
                            }

                            /*
                            var existing = localStorage.getItem('users');
                                existing = existing ? JSON.parse(existing) : {};
                            var match = false;
                            if (value.length === 4) {
                                for (var i in existing) {
                                    if (existing[i].PinCode == value) {
                                        signinbyPIN(value, function(data){
                                            //console.log(data);
                                            localStorage.setItem('ContextID', data.Contexts[0].ID);
                                            localStorage.setItem('username', existing[i].username);
                                            localStorage.setItem('UserId', data.UserId);
                                            localStorage.setItem('FirstName', data.FirstName);
                                            localStorage.setItem('LastName', data.LastName);
                                            localStorage.setItem('Token', data.Token.replace('nvt.api.',''));
                                            page.detail.router.navigate('/dashboard/');
                                            getConfig();
                                        });
                                        match = true;
                                        break;
                                    } 
                                }
                                if(!match) {
                                    app.dialog.alert('Wrong passcode! Please try again.', function () {
                                        app.views.main.router.navigate(app.views.main.router.currentRoute.url, {
                                            ignoreCache: true,
                                            reloadCurrent: true
                                        });
                                    });
                                }
                            }
                            */
                        }
                    }
                });
            },
        },
    },{
        path: '/dashboard/',
        url: './pages/dashboard.html',
        on: {
            pageInit: function() {
                app.preloader.hide();
                $('#myName').html(localStorage.getItem('StoreName')+'<br>'+localStorage.getItem('FirstName') + ' ' + localStorage.getItem('LastName'));
                $('.connectTo span').text(localStorage.getItem('defaultPrinter'));

                /*
                if(!app.device.desktop) {
                    runPrinter('connect');
                }
                */

                getToday();
                //getConfig();

                /*
                var allContextIds = localStorage.getItem('allContextIds');
                if(allContextIds !== null && allContextIds) {
                    $('.store-select').parent().removeClass('hide');
                }
                */

                $('input.keypad').focus();
                window.setTimeout(function() {
                    if (window.cordova) {
                        Keyboard.hide();
                    }
                }, 1);
                $('#trick').on('click', function(e) {
                    e.preventDefault();
                    $('input.keypad').removeAttr("disabled");
                    $('input.keypad').focus();
                    window.setTimeout(function() {
                        if (window.cordova) {
                            Keyboard.hide();
                        }
                    }, 50);
                });

                /*
                $('#despatched .logout').on('click', function(e) {
                    app.dialog.confirm('Are you sure you want to logout?', function () {
                        window.logout();
                    });
                });
                */

                $("form").keypress(function(e) {
                    //Enter key
                    if (e.which == 13) {
                        e.preventDefault();
                        return false;
                    }
                });
                $('#keyboard .keypad').on('change keydown paste input', function() {
                    var ord = $(this).val();
                    if (ord.length == 10) {
                        searchForOrder(ord); 
                    }
                });
                $('#keypad').keypad();
                if(waitToGetConfig) {
                    $('#tastatura').css('opacity', '0.1');
                }
            }
        }
    },{
        path: '/settings/',
        url: './pages/settings.html',
        on: {
            pageInit: function() {
            	//$('.profile-email').val(localStorage.getItem('username'));
            	$('.profile-first-name').val(localStorage.getItem('FirstName'));
            	$('.profile-last-name').val(localStorage.getItem('LastName'));
            	$('.defaultIP').val(localStorage.getItem('defaultIP'));
                $('#printer option[value="'+localStorage.getItem('defaultPrinter')+'"]').attr("selected", true);
                $('.connectPrinter span').text($('#printer option:selected').text());
                localStorage.setItem('defaultPrinter', $('#printer option:selected').val());
                if($('#printer option:selected').text() !== 'Zebra') {
                    $('.sendZebraTestLabel').hide();
                }
                $('#printer').on('change', function() {
                    var selected = $(this).find('option:selected').text();
                    $('.connectPrinter span').text(selected);
                    if(selected == 'Zebra') {
                        $('.sendZebraTestLabel').show();
                    } else {
                        $('.sendZebraTestLabel').hide();
                    }
                    localStorage.setItem('defaultPrinter', $(this).val());
                });
                $('.defaultIP').on('input', function() {
                    localStorage.setItem('defaultIP', $(this).val());
                });
                getProfile();
                if(localStorage.getItem('defaultTimeout') !== null){
                    $('.timeout').val(localStorage.getItem('defaultTimeout'));
                }
                $('.timeout').on('input', function() {
                    var min = $(this).val();
                    if(min == '' || min == '0') {
                        min = 5
                    }
                    localStorage.setItem('defaultTimeout', min);
                });
                $('#set-timeout').on('click', function(e) {
                    var min = parseInt($('.timeout').val());
                    if(min < 1){
                        app.dialog.alert('Number should be higher than 1');
                        $('.timeout').val('5');
                        localStorage.setItem('defaultTimeout', 5);
                        return;
                    }
                    localStorage.setItem('defaultTimeout', min);
                    app.dialog.alert('Timeout set to '+min+' minutes');
                });
                $('#label-edit').on('click', function(e) {
                    e.preventDefault();
                    var lblEditor = app.popup.create({
                        content: `<div class="popup labelEditor-popup popup-tablet-fullscreen">
                            <div class="view">
                                <div class="page">
                                    <div class="navbar">
                                        <div class="navbar-bg"></div>
                                        <div class="navbar-inner">
                                            <div class="title">Label Editor <small>(left <span class="l">50</span>, top <span class="t">0</span>)</small></div>
                                            <div class="right">
                                                <a class="link labelSave popup-close"><i class="icon f7-icons" style="margin-right:5px">floppy_disk</i> Save</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="page-content" id="drag"> 
                                        <button class="button button-round up"><i class="icon f7-icons">square_arrow_up_fill</i></button>
                                        <button class="button button-round down"><i class="icon f7-icons">square_arrow_down_fill</i></button>
                                        <button class="button button-round left"><i class="icon f7-icons">square_arrow_left_fill</i></button>
                                        <button class="button button-round right"><i class="icon f7-icons">square_arrow_right_fill</i></button>
                                        <div class="holder">
                                            <div class="lbl"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>`,
                        on: {
                            opened: function () {

                                var timeOut = 0,
                                    t = $('.navbar .title .t'),
                                    l = $('.navbar .title .l'),
                                    holder = $('#drag .holder'),
                                    lbl = $('#drag .lbl'),
                                    //defLeft = lbl.css('margin-left'),
                                    //defTop = lbl.css('margin-top'),
                                    labelEditor = JSON.parse(localStorage.getItem('labelEditor'));




                                var childPos = lbl.offset();
                                var parentPos = holder.offset();

                                var startPositionLeft = childPos.left - parentPos.left - parseInt(holder.css("border-left-width"));
                                var startPositionTop = childPos.top - parentPos.top - parseInt(holder.css("border-top-width"));

                                if(labelEditor == null){
                                    t.text(startPositionTop);
                                    l.text(startPositionLeft);
                                } else {
                                    t.text(labelEditor.top);
                                    l.text(labelEditor.left);
                                }

                        
                                var newTopPosition = startPositionTop;
                                var newLeftPosition = startPositionLeft;

                                var maxTop = holder.height() - lbl.height();
                                var maxLeft = holder.width() - lbl.width();







                                $('#drag .up').on('mousedown touchstart', function(e) {
                                    if(newTopPosition > 0){
                                        timeOut = setInterval(function(i){
                                            if(newTopPosition > 0){
                                                newTopPosition = newTopPosition - 1;
                                                t.text(newTopPosition);
                                                lbl.css({'bottom':'auto','top': parseInt(lbl.css('top'))-1+'px'});
                                            }
                                        }, 100);
                                        lbl.css({'bottom':'auto','top': parseInt(lbl.css('top'))-1+'px'});
                                        newTopPosition = newTopPosition - 1;
                                        t.text(newTopPosition);
                                    }
                                }).bind('mouseup mouseleave touchend', function() {
                                    clearInterval(timeOut);
                                });

                                $('#drag .down').on('mousedown touchstart', function(e) {
                                    if(newTopPosition < maxTop){
                                        timeOut = setInterval(function(){
                                            if(newTopPosition < maxTop){
                                                newTopPosition = newTopPosition + 1;
                                                t.text(newTopPosition);
                                                lbl.css({'top':'auto','bottom':parseInt(lbl.css('bottom'))-1+'px'});
                                            }
                                        }, 100);
                                        lbl.css({'top':'auto','bottom': parseInt(lbl.css('bottom'))-1+'px'});
                                        newTopPosition = newTopPosition + 1;
                                        t.text(newTopPosition);
                                    }
                                }).bind('mouseup mouseleave touchend', function() {
                                    clearInterval(timeOut);
                                });

                                $('#drag .left').on('mousedown touchstart', function(e) {
                                    if(newLeftPosition > 0){
                                        timeOut = setInterval(function(){
                                            if(newLeftPosition > 0){
                                                newLeftPosition = newLeftPosition - 1;
                                                l.text(newLeftPosition);
                                                lbl.css({'right':'auto','left':parseInt(lbl.css('left'))-1+'px'});
                                            }
                                        }, 100);
                                        lbl.css({'right':'auto','left':parseInt(lbl.css('left'))-1+'px'});
                                        newLeftPosition = newLeftPosition - 1;
                                        l.text(newLeftPosition);
                                    }
                                }).bind('mouseup mouseleave touchend', function() {
                                    clearInterval(timeOut);
                                });

                                $('#drag .right').on('mousedown touchstart', function(e) {
                                    if(newLeftPosition < maxLeft){
                                        timeOut = setInterval(function(){
                                            if(newLeftPosition < maxLeft){
                                                newLeftPosition = newLeftPosition + 1;
                                                l.text(newLeftPosition);
                                                lbl.css({'left':'auto','right':parseInt(lbl.css('right'))-1+'px'});
                                            }
                                        }, 100);
                                        lbl.css({'left':'auto','right':parseInt(lbl.css('right'))-1+'px'});
                                        newLeftPosition = newLeftPosition + 1;
                                        l.text(newLeftPosition);
                                    }
                                }).bind('mouseup mouseleave touchend', function() {
                                    clearInterval(timeOut);
                                });

                                $('.labelSave').on('click', function(e) {
                                    var label = {
                                        left: l.text(),
                                        top: t.text()
                                    }
                                    localStorage.setItem('labelEditor', JSON.stringify(label));
                                });
                            }
                        }
                    }).open();
                });
            }
        }
    },{
        path: '/pin/',
        url: './pages/pin.html',
        on: {
            pageInit: function(page) {
                var numpadInline = app.keypad.create({
                    inputEl: '#demo-numpad-inline',
                    containerEl: '#numpad-inline-container',
                    toolbar: false,
                    valueMaxLength: 4,
                    dotButton: false,
                    formatValue: function (value) {
                        value = value.toString();
                        return ('****').substring(0, value.length) + ('____').substring(0, 4 - value.length);
                    },
                    on: {
                        change(keypad, value) {
                            value = value.toString();
                            //var existing = localStorage.getItem('users');
                            //    existing = existing ? JSON.parse(existing) : {};
                            if (value.length === 4) {
                                setUserPIN(value, function(e){
                                    if(e === 'OK') {
                                        /*
                                        for (var i in existing) {
                                            if (existing[i].UserId == localStorage.getItem('UserId')) {
                                                existing[i].PinCode = value;
                                                break;
                                            }
                                        }
                                        localStorage.setItem('users', JSON.stringify(existing));
                                        */
                                        localStorage.setItem('PinCode', value);
                                        page.detail.router.navigate('/dashboard/');
                                    } else {
                                        numpadInline.setValue('');
                                    }
                                });
                            }
                        }
                    }
                });
            }
        }
    },{
        path: '/changepin/',
        url: './pages/changepin.html',
        on: {
            pageInit: function(page) {
                var numpadInline = app.keypad.create({
                    inputEl: '#demo-numpad-inline',
                    containerEl: '#numpad-inline-container',
                    toolbar: false,
                    valueMaxLength: 4,
                    dotButton: false,
                    formatValue: function (value) {
                        value = value.toString();
                        return ('****').substring(0, value.length) + ('____').substring(0, 4 - value.length);
                    },
                    on: {
                        change(keypad, value) {
                            value = value.toString();
                            if(value.length === 4) {
                                setUserPIN(value, function(e){
                                    if(e === 'OK') {
                                        localStorage.setItem('PinCode', value);
                                        //existing[i].PinCode = value;
                                        //localStorage.removeItem('newPinCode');
                                        //localStorage.setItem('users', JSON.stringify(existing));
                                        //page.detail.router.navigate('/settings/');
                                        console.log(e);
                                        return;
                                    } 
                                    numpadInline.setValue('');
                                });
                            }
                            
                            /*
                            var match = false;
                            var existing = JSON.parse(localStorage.getItem('users'));
                            var newPinCode = JSON.parse(localStorage.getItem('newPinCode'));
                            if(newPinCode && newPinCode !== null){
                                if(value.length === 4) {
                                    for (var i in existing) {
                                        if (existing[i].UserId == localStorage.getItem('UserId')) {
                                            setUserPIN(value, function(e){
                                                if(e === 'OK') {
                                                    existing[i].PinCode = value;
                                                    localStorage.removeItem('newPinCode');
                                                    localStorage.setItem('users', JSON.stringify(existing));
                                                    //page.detail.router.navigate('/settings/');
                                                    console.log(e)
                                                } else {
                                                    numpadInline.setValue('');
                                                }
                                            });
                                            break;
                                        }
                                    }
                                }
                                return 
                            } 
                            if(value.length === 4) {
                                for (var i in existing) {
                                    if (existing[i].PinCode == value) {
                                        match = true;
                                        $('.login-screen-title').text('New Pin Code');
                                        localStorage.setItem('newPinCode', true);
                                        numpadInline.setValue('');
                                        break;
                                    }
                                }
                                if(!match) {
                                    app.dialog.alert('Wrong passcode! Please try again.', function () {
                                        app.views.main.router.navigate(app.views.main.router.currentRoute.url, {
                                            ignoreCache: true,
                                            reloadCurrent: true
                                        });
                                    });
                                }
                            }
                            */
                        }
                    }
                });
            }
        }
    },{
        path: '/orders/',
        url: './pages/orders.html',
        on: {
            pageInit: function() {
            	getOrders('2019-09-01', 0);
            }
        }
    },{
        // Default route (404 page). MUST BE THE LAST
        path: '(.*)',
        url: './pages/404.html',
    },
];
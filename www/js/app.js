var $$ = Dom7;
var in_barcode_scan = false;
var printer = null;
var refresh;
var defaultTimeout = 5; // 5 min default
var api = 'https://api.nventree.co.uk/api/v1/';
var devId = '9C7BFBC8-DB88-45A5-AE7D-817B61C014B8'; // nvt
var appId = '1F50C871-F4CB-4F09-8D47-4246CC624955'; // nvt 
// var appId = '4739E08B-7DE0-449F-B203-EA807D4562EC'; // jsi

// https://api.nventree.co.uk/Help/Api/POST-api-vversion-shipment-AddParcel_orderId
// http://framework7.io/packages/core/js/framework7.bundle.min.js
// https://stackoverflow.com/questions/40375555/start-android-activity-from-cordova-plugin

(function($) {
    $.fn.hideKeyboard = function() {
        var inputs = this.filter("input").attr('readonly', 'readonly'); 
        var textareas = this.filter("textarea").attr('disabled', 'true'); 
        setTimeout(function() {
            inputs.blur().removeAttr('readonly');
            textareas.blur().removeAttr('disabled');
        }, 100);
        return this;
    };
}(jQuery));

(function($) {
    (function(pluginName) {
        var defaults = {
            inputField: 'input.keypad',
            buttonTemplate: '<button></button>',
            submitButtonText: '<i class="f7-icons">barcode</i><span>SCAN</span>',
            deleteButtonText: '<i class="f7-icons">delete_left</i><span>DELETE</span>',
            submitButtonClass: 'button scan',
            deleteButtonClass: 'button delete'
        };
        $.fn[pluginName] = function(options) {
            options = $.extend(true, {}, defaults, options);
            return this.each(function() {
                var elem = this,
                    $elem = $(elem),
                    $input = jQuery.type(options.inputField) == 'string' ? $(options.inputField) : options.inputField,
                    $form = $input.parents('form').length ? $($input.parents('form')[0]) : $elem;
                var numbers = Array.apply(null, Array(9)).map(function(_, i) {
                    return $(options.buttonTemplate).html(i + 1).addClass('button number btn'+(i+1));
                });
                numbers.push($(options.buttonTemplate).html(options.submitButtonText).addClass(options.submitButtonClass));
                numbers.push($(options.buttonTemplate).html("0").addClass('button number btn0'));
                numbers.push($(options.buttonTemplate).html(options.deleteButtonText).addClass(options.deleteButtonClass));
                $elem.html(numbers).addClass('keypad');
                $elem.find('.number').on('click', function(e) {
                    $input.val($input.val() + $(e.target).text());
                    $input.trigger('change');
                });
                $elem.find('.delete').on('click', function(e) {
                    $input.val($input.val().slice(0, -1));
                    $input.trigger('change');
                });
                $elem.find('.scan').on('click', function(e) {
                    scanBarcode();
                });
            });
        };
        $.fn[pluginName].defaults = defaults;
    })('keypad');
})(jQuery);

Framework7.use(Framework7Keypad);

var app = new Framework7({
    root: '#app', 
    id: 'com.justapplications.nventree', // App bundle ID
    name: 'Nventree', // App name
    theme: 'auto', 
    routes: routes,
    lazy: {
        threshold: 50,
    },
    on: {
        init: function() {
            document.addEventListener('deviceready', onDeviceReady, false);
            document.addEventListener('offline', checkConnection, false);
            document.addEventListener('online', checkConnection, false);
        },
        pageInit: function() {
            var panelLeft = $('.panel-left .no-lockscreen'),
            	curPage = app.views.main.router.currentPageEl;
                curPage = $(curPage).data('name');
            if(curPage === 'lockscreen') {
                panelLeft.hide();
            } else {
                panelLeft.show();
            }
            if (curPage === 'home') {
            	if (localStorage.getItem('Token') == null) {
                    logout();
                    return; 
                }
                if (localStorage.getItem('PinCode') !== null) {



                    // dev purposes only
                    if(app.device.desktop) {
                        app.views.main.router.navigate('/dashboard/');
                        getConfig();
                        setTimeout(function(){
                            $('input.keypad').val('000000');
                        }, 1);
                        return;
                    }
                    
                    
                	app.views.main.router.navigate('/passcode/');
                	return;
                }
                app.views.main.router.navigate('/dashboard/');
            }
        }
    }
});

var mainView = app.views.create('.view-main', {
	url: '/',
	animate:false
});

function onDeviceReady() {
    if (app.device.android || cordova.platformId == 'android') {
        document.addEventListener('backbutton', onBackKeyDown, false);
    }
}

function onBackKeyDown() {
    var curPage = app.views.main.router.currentPageEl;
        curPage = $(curPage).data('name');
    if (in_barcode_scan) {
        in_barcode_scan = false;
        return;
    }
    if ($('.modal-in').length > 0) {
        //app.popup.close();
        //app.dialog.close();
        return;
    } 
    if(curPage == 'offline' || curPage == 'passcode' || curPage == 'pin' || curPage == 'changepin'){
        // stop getting to prev page
        return;
    } 
    if (curPage == 'dashboard' && localStorage.getItem('Token') !== null){
        app.dialog.confirm('Are you sure you want to logout?', function () {
            logout();
        });
        return;
    } 
    if(curPage == 'home' && localStorage.getItem('Token') == null) {
        app.dialog.confirm('Are you sure you want to exit?', function () {
            navigator.app.exitApp();
        });
        return;
    }
    app.views.main.router.back();
}

function checkConnection() {
    var networkState = navigator.connection.type;
    if (networkState !== Connection.NONE) {
    	app.dialog.close();
        if (localStorage.getItem('Token') !== null) {
        	if(localStorage.getItem('PinCode') !== null){
        		app.views.main.router.navigate('/passcode/');
        		return;
        	}
            app.views.main.router.navigate('/dashboard/');
            return;
        } 
        logout();
    } 
    app.views.main.router.navigate('/offline/');
    return;
}

function checkInputs(inputs, button) {
    var isValid = true;
    $(inputs).each(function() {
        if ($(this).is(':visible')) {
            if ($(this).val() === '' || $(this).hasClass('input-invalid')) {
                $(button).prop('disabled', true);
                isValid = false;
                return false;
            }
        }
    });
    if (isValid) {
        $(button).prop('disabled', false);
    }
    return isValid;
}

function showWinDialog(text) {
    if (dialog) {
        dialog.content = text;
        return;
    }
    dialog = new Windows.UI.Popups.MessageDialog(text);
    dialog.showAsync().done(function() {
        dialog = null;
    });
};

function toast(text) {
    var isMac = navigator.userAgent.toLowerCase().includes('macintosh');
    text = text !== null ? text : 'finished or canceled';
    setTimeout(function() {
        if (window.Windows !== undefined) {
            showWinDialog(text);
            return;
        } 
        if (!isMac && window.plugins && window.plugins.toast) {
            window.plugins.toast.showShortBottom(String(text));
            return;
        } 
        app.dialog.alert(text);
    }, 500);
};

function logout() {
    var cId = localStorage.getItem('ContextID');
	var ip = localStorage.getItem('defaultIP');
    localStorage.clear();
    localStorage.setItem('f7router-view_main-history', ["/"]);
    localStorage.setItem('defaultIP', ip);
    localStorage.setItem('ContextID', cId);
    app.views.main.router.navigate('/');
    return;
}

function scanQRCode(callback){

    // development only
    if(app.device.desktop) {
        var cid = 'd6a4e29c-308b-461c-a787-3e42c629930c'; // buyaparcel
        var pin = '1234'; // adrian
        signinbyPIN(cid, pin, function(data){
            console.log(data);
            if(data.Success){
                localStorage.setItem('ContextID', data.Contexts[0].ID);
                localStorage.setItem('StoreName', data.Contexts[0].Name);
                //localStorage.setItem('username', existing[i].username);
                localStorage.setItem('UserId', data.UserId);
                localStorage.setItem('FirstName', data.FirstName);
                localStorage.setItem('LastName', data.LastName);
                localStorage.setItem('PinCode', pin);
                localStorage.setItem('Token', data.Token.replace('nvt.api.',''));
                app.views.main.router.navigate('/dashboard/');
                getConfig();
                return;
            }
        });
        return;
    }
    // ######

    cordova.plugins.barcodeScanner.scan(function (result) {
        in_barcode_scan = true;
        if (!result.cancelled) {
            var scan = result.text;
            console.log(scan);
            if (scan.indexOf('/') > -1) {
                var cid = scan.split('/')[0]
                var pin = scan.split('/')[1]
                signinbyPIN(cid, pin, function(data){
                    console.log(data);
                    if(data.Success){
                        localStorage.setItem('ContextID', data.Contexts[0].ID);
                        localStorage.setItem('StoreName', data.Contexts[0].Name);
                        //localStorage.setItem('username', existing[i].username);
                        localStorage.setItem('UserId', data.UserId);
                        localStorage.setItem('PinCode', pin);
                        localStorage.setItem('FirstName', data.FirstName);
                        localStorage.setItem('LastName', data.LastName);
                        localStorage.setItem('Token', data.Token.replace('nvt.api.',''));
                        app.views.main.router.navigate('/dashboard/');
                        getConfig();
                        return;
                    }
                    app.dialog.alert(data.FailureReason);
                });
                return;
            }
            localStorage.setItem('ContextID', scan);
            toast('No pin code. Please login with username.');
            app.loginScreen.open('.new-login-screen');
        }
    }, function (error) {
        console.log(error);
        app.dialog.alert('Scanning failed: ' + error);
    }, {
        preferFrontCamera : false, // iOS and Android
        showFlipCameraButton : true, // iOS and Android
        showTorchButton : false, // iOS and Android
        torchOn: false, // Android, launch with the torch switched on (if available)
        saveHistory: true, // Android, save scan history (default false)
        prompt : "Place the Nventree QR Code inside the scan area", // Android
        resultDisplayDuration: 0, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
        formats : "QR_CODE", // default: all but PDF_417 and RSS_EXPANDED
        orientation : "landscape", // Android only (portrait|landscape), default unset so it rotates with the device
        disableAnimations : true, // iOS
        disableSuccessBeep: false // iOS and Android
    });
}

function scanLabel(orderId, orderNr, curierId){
	var printer = localStorage.getItem('defaultPrinter');
    var ip = localStorage.getItem('defaultIP');
    if(printer == 'null'){
        app.dialog.confirm('Please select a default printer in Settings.', function () {
            app.views.main.router.navigate('/settings/');
        });
        return;
    }
    if(ip == 'null'){
        app.dialog.confirm('Please add printer IP in Settings.', function () {
            app.views.main.router.navigate('/settings/');
        });
        return;
    }

    var trackingNr = '000';
    var despatched = true;
    createShipment(orderId, orderNr, curierId, trackingNr, despatched);

    /*
	cordova.plugins.barcodeScanner.scan(function(result) {
        in_barcode_scan = true;
		//console.log(result);
        if (!result.cancelled) {
            var trackingNr = result.text;
            var despatched = true;
            //console.log(trackingNr);
            createShipment(orderId, orderNr, curierId, trackingNr, despatched);
        	return false;
        }
    }, function(error) {
        console.log(error);
        app.dialog.alert('Scanning failed: ' + error);
    }, {
        preferFrontCamera: true, // iOS and Android
        showFlipCameraButton: false, // iOS and Android
        showTorchButton: false, // iOS and Android
        torchOn: false, // Android, launch with the torch switched on (if available)
        saveHistory: true, // Android, save scan history (default false)
        prompt: 'Place a barcode inside the scan area', // Android
        resultDisplayDuration: 0, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
        //formats: 'QR_CODE', // default: all but PDF_417 and RSS_EXPANDED
        orientation: 'landscape', // Android only (portrait|landscape), default unset so it rotates with the device
        disableAnimations: true, // iOS
        disableSuccessBeep: false // iOS and Android
    });
    */
}  

function scanBarcode() {
    $('input.keypad').val('');
    cordova.plugins.barcodeScanner.scan(function(result) {
        in_barcode_scan = true;
        //console.log(result);
        if (!result.cancelled) {
            var o = result.text;
                o = o.replace(/\D/g, '');
                o = pad(o, 10);
        	$('input.keypad').attr("disabled", true).val(o);
        	searchForOrder(o);
        	return false;
        }
    }, function(error) {
        console.log(error);
        //app.dialog.alert('Scanning failed: ' + error);
    }, {
        preferFrontCamera: true, // iOS and Android
        showFlipCameraButton: false, // iOS and Android
        showTorchButton: false, // iOS and Android
        torchOn: false, // Android, launch with the torch switched on (if available)
        saveHistory: true, // Android, save scan history (default false)
        prompt: 'Place a barcode inside the scan area', // Android
        resultDisplayDuration: 0, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
        //formats: 'QR_CODE', // default: all but PDF_417 and RSS_EXPANDED
        orientation: 'landscape', // Android only (portrait|landscape), default unset so it rotates with the device
        disableAnimations: true, // iOS
        disableSuccessBeep: false // iOS and Android
    });
}

function runPrinter(action){
    console.log('runPrinter');

    var printer = $('#printer').val();
    var defaultIP = $('.defaultIP').val();
    var curPage = app.views.main.router.currentPageEl;
    if($(curPage).data('name') == 'dashboard'){
        if(printer == undefined){
            printer = localStorage.getItem('defaultPrinter');
        }
        if(defaultIP == undefined){
            defaultIP = localStorage.getItem('defaultIP');
        }
        if(printer == undefined || defaultIP == undefined || defaultIP == 'null' || defaultIP == ''){
            $('.connectTo').hide();
            app.dialog.confirm('Please select default printer and IP address in Settings.', function () {
                app.views.main.router.navigate('/settings/');
            });
            return;
        }
    }

	if(!checkInputs('.defaultIP', '.connectPrinter')) {
		toast('Please enter a valid IP address.');
		return;
	}

	switch(printer){
		case "zebra":
			var port = '9100';
			if(action === 'connect'){
                app.preloader.show();
				window.plugins.zebraprintwifi.connect(port, defaultIP, function(data) {
			        //console.log('Connected');
			        //console.log(data);
                    app.preloader.hide();
			    }, function(error) {
			        console.log(error);
                    app.preloader.hide();
			    });
			}
			if(action === 'testprint'){
                app.preloader.show();
				window.plugins.zebraprintwifi.testprint(port, defaultIP, function(data) {
			        //console.log('Test printed.');
			        //console.log(data);
                    app.preloader.hide();
			    }, function(error) {
			        console.log(error);
                    app.preloader.hide();
			    });
			}
			if(action === 'status'){
                app.preloader.show();
				window.plugins.zebraprintwifi.status(port, defaultIP, function(data) {
			        //console.log('Status');
			        //console.log(data);
                    app.preloader.hide();
			    }, function(error) {
			        console.log(error);
                    app.preloader.hide();
			    });
			}
			if(action === 'disconnect'){
                app.preloader.show();
				window.plugins.zebraprintwifi.disconnect(port, defaultIP, function(data) {
			        //console.log('Disconnected');
			        //console.log(data);
                    app.preloader.hide();
			    }, function(error) {
			        console.log(error);
                    app.preloader.hide();
			    });
			}
			break;
		case "cls521":
			localStorage.setItem('defaultIP', defaultIP);
			if(action === 'connect'){
				//console.log('connect')
				window.plugins.clsPrinter.connect('Connected!', defaultIP, function() {
			        //console.log('Connect!');
			    }, function(err) {
			        console.log('Uh oh... ' + err);
			    });
			}
			break;
	    //default:
	}
}

function printLabel(img){
    var printer = localStorage.getItem('defaultPrinter');
    var ip = localStorage.getItem('defaultIP');
    //var img = localStorage.getItem('labelNativeURL');
    var pathToFile = cordova.file.cacheDirectory + img;
    if(printer === 'zebra'){
        window.plugins.zebraprintwifi.print(pathToFile, ip, function(data) {
            //console.log('Label Printed!');
            //app.popup.close();
            app.preloader.hide();
            //console.log(data);
        }, function(err) {
            app.dialog.alert('Uh oh... ' + err);
            app.preloader.hide();
        });
    } 
    if(printer === 'cls521'){
        pathToFile = cordova.file.cacheDirectory.replace('file://', '') + img;
        window.plugins.clsPrinter.print(pathToFile, ip, function() {
            //console.log('Label Printed!');
            //app.popup.close();
            app.preloader.hide();
        }, function(err) {
            app.dialog.alert('Uh oh... ' + err);
            app.preloader.hide();
        });
    }
}

function updateProfile(){
    app.preloader.show();
    $.ajax({
        type: 'POST',
        url: api + 'company',
        cache: false,
        crossDomain: true,
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        processData: true,
        data: {
        	EmailAddress: $('.profile-email').val(),
        	CompanyName: $('.profile-company-name').val(),
        	TelephoneNumber: $('.profile-phone').val(),
        	RegistrationNumber: $('.profile-registration-number').val(),
        	AddressLine1: $('.profile-address-line1').val(),
        	AddressLine2: $('.profile-address-line2').val(),
        	AddressLine3: $('.profile-address-line3').val(),
        	Country: $('.profile-country').val(),
        	County: $('.profile-county').val(),
        	Postcode: $('.profile-postcode').val(),
        	WebsiteURL: $('.profile-website-url').val(),
        	VATNumber: $('.profile-vat-number').val()
        },
        headers: {
        	"ApiToken": "nvt.api."+localStorage.getItem('Token'),
        	"DeveloperID": devId,
        	"ContextID": localStorage.getItem('ContextID')
        },
        success: function(data, status) {
        	//console.log(data);
        	app.preloader.hide();
        	//if(data === 'Success');
        	toast(data);
        },
        error: function(xhr, textStatus, errorThrown) {
        	app.preloader.hide();
        	toast(errorThrown);
        	//app.dialog.alert('Error.');
        },
    });
}

function signin(){
    if(!checkInputs('input[name="username"], input[name="password"]', '#login-screen .login-button')) {
		toast('Please complete all fields.');
		return;
	}
    app.preloader.show();
    var phone;
    if(!app.device.desktop) {
        phone = {
            uuid: device.uuid,
            manufacturer: device.manufacturer,
            model: device.model
        }
        console.log(phone);
    }
    $.ajax({
            type: 'POST',
            url: api + 'auth/signin',
            cache: false,
            crossDomain: true,
            contentType: "application/x-www-form-urlencoded",
            dataType: "json",
            processData: true,
            data: {
                Device: phone,
                Username: $('input[name="username"]').val(),
                Password: $('input[name="password"]').val(),
                DeveloperID: devId,
                ApplicationID: appId
            },
            success: function(data) {
                console.log(data);

                app.preloader.hide();
                
                if (data.Success) {

                    localStorage.setItem('username', $('input[name="username"]').val());

                    if(localStorage.getItem('defaultIP') == null) {
                    	localStorage.setItem('defaultIP', '0.0.0.0');
                    }

                    localStorage.setItem('UserId', data.UserId);
                    localStorage.setItem('Token', data.Token.replace('nvt.api.',''));
                    localStorage.setItem('FirstName', data.FirstName);
                    localStorage.setItem('LastName', data.LastName);

                    localStorage.setItem('StoreName', data.Contexts[0].Name);

                    app.loginScreen.close('.new-login-screen');

                    var stores = data.Contexts;
                   
                    for (var i in stores) {
                        if (stores[i].ID == localStorage.getItem('ContextID')) {
                        	if(stores[i].PinCode !== null ) {
                                console.log(stores[i].PinCode);
                                localStorage.setItem('PinCode', stores[i].PinCode);
                                app.views.main.router.navigate('/dashboard/');

                                getPlatforms();
                                getCouriers();
                                getConfig();

                            } else {
                                app.views.main.router.navigate('/pin/');
                            }
                            break;
                        } 
                    }

                    return;
                } 
                toast(data.FailureReason);

                /*
                if (data.Success) {
                	var users = localStorage.getItem('users');

                    //var cid = '';

                	if(users !== null && users) {
                		users = JSON.parse(users);
                	} else {
                		users = []
                	}

                	//console.log(users);

                	var userExist = users.some(function(o){
                		return o["UserId"] === data.UserId;
                	});

                    //console.log('userExist in mobile app: '+userExist);

                    //console.log(data.Contexts[0].PinCode);

                    var stores = data.Contexts;
                    var pin = false;

                    for (var i in stores) {
                        if (stores[i].ID == localStorage.getItem('ContextID')) {
                        	if(stores[i].PinCode !== null ) {
                                localStorage.setItem('PinCode', true);
                            }
                            break;
                        } 
                    }

                	if(!userExist){
	                	users.push({
	                		'username': $('input[name="username"]').val(),
	                		'UserId': data.UserId,
	                		'FirstName': data.FirstName,
	                		'LastName': data.LastName,
	                		'Token': data.Token.replace('nvt.api.',''),
	                		'PinCode': ''
	                	});
	                }

                	localStorage.setItem('users', JSON.stringify(users));

                	var loggedInUsers = users.length;
                	//console.log('loggedInUsers: '+loggedInUsers);
                	
                    localStorage.setItem('username', $('input[name="username"]').val());

                    var ip = localStorage.getItem('defaultIP');
                    //console.log(ip);
                    if(ip == null) {
                    	localStorage.setItem('defaultIP', '0.0.0.0');
                    }

                    localStorage.setItem('UserId', data.UserId);
                    localStorage.setItem('Token', data.Token.replace('nvt.api.',''));
                    localStorage.setItem('FirstName', data.FirstName);
                    localStorage.setItem('LastName', data.LastName);    

                	if(loggedInUsers > 1){
                		app.loginScreen.close();
                        var users = JSON.parse(localStorage.getItem('users'));
                        $.each(data.Contexts, function(i,v){
                            if(v.ID == localStorage.getItem('ContextID')){
                                var existingPing = v.PinCode;
                                if(existingPing !== null || existingPing !== ""){
                                    for (var i in users) {
                                        if (users[i].username == localStorage.getItem('username')) {
                                            users[i].PinCode = v.PinCode;
                                            break;
                                        }
                                    }
                                    localStorage.setItem('users', JSON.stringify(users));
                                }
                            }
                        });
                        for (var i in users) {
                            if (users[i].username == localStorage.getItem('username')) {
                                if(users[i].PinCode === "") {
                                    app.views.main.router.navigate('/pin/');
                                } else {
                                    app.views.main.router.navigate('/dashboard/');
                                }
                                getPlatforms();      
                                break; 
                            }
                        }
                	} else {
                        if(data.Contexts.length > 1){
                            var cIds = []
                            var sel = '';
                            $.each(data.Contexts, function(i,v){
                                if(v.DisabledReason.length){
                                    //console.log('disabled '+v.Name);
                                } else {
                                    cIds.push({Name:v.Name,Id:v.ID});
                                    sel += '<option value="'+v.ID+'">'+v.Name+'</option>';
                                }
                            });
                            $('#cIds').append(sel);
                            localStorage.setItem('allContextIds', JSON.stringify(cIds));

                            // SET FIRST STORENAME + CID
                            // IN CASE IF USER DONESNT SELECT NOTHING AND REOPENS THE APP
                            localStorage.setItem('StoreName', data.Contexts[0].Name);
                            localStorage.setItem('ContextID', data.Contexts[0].ID); //cId

                            var smartSelect = app.smartSelect.create({
                                el: '.smart-select',
                                openIn: 'popup',
                                popupCloseLinkText: 'Select',
                                popupTabletFullscreen:true,
                                closeOnSelect: false,
                                routableModals: false,
                                searchbar: true,
                                pageTitle: 'Select Store',
                                on: {
                                    close:function(e){
                                        //app.preloader.show();
                                        localStorage.setItem('StoreName', $(smartSelect.valueEl).text());
                                        localStorage.setItem('ContextID', smartSelect.getValue()); //cId
                                        //localStorage.setItem('Token', data.Token.replace('nvt.api.',''));

                                        var pin = null;
                                        $.each(data.Contexts, function(i,v){
                                            //console.log(v);
                                            if(v.ID === smartSelect.getValue()){
                                                //console.log('pin '+v.PinCode);
                                                if(v.PinCode !== null) {
                                                    localStorage.setItem('PinCode', true);
                                                }
                                                pin = v.PinCode;
                                                var users = JSON.parse(localStorage.getItem('users'));
                                                for (var i in users) {
                                                    if (users[i].username == localStorage.getItem('username')) {
                                                        users[i].PinCode = pin;
                                                        break; 
                                                    }
                                                }
                                                localStorage.setItem('users', JSON.stringify(users));
                                            }
                                        });
                                        //localStorage.setItem('users', users);
                                        //console.log('aici');
                                        //console.log(pin);
                                        if(pin == null) {
                                        	app.views.main.router.navigate('/pin/');
                                        	app.preloader.hide();
                                        } else {
                                        	app.views.main.router.navigate('/dashboard/');
                                        }
                                        getPlatforms();
                                        getCouriers();
                                        getConfig();
                                    }
                                }
                            });
                            smartSelect.open();
                        } else {
                            //console.log('single store - cid');
                            if(data.Contexts[0].DisabledReason.length){
                                app.dialog.alert('Store Disabled!');
                                //console.log('logout 3');
                                logout();
                            } else {
                            	//console.log('pe aici');
                                localStorage.setItem('StoreName', data.Contexts[0].Name);
                                localStorage.setItem('ContextID', data.Contexts[0].ID); //cId
                                localStorage.setItem('Token', data.Token.replace('nvt.api.',''));
                                var hasPin = data.Contexts[0].PinCode
                                //console.log(hasPin);
                                if(hasPin === '' || hasPin == null){
                                    //console.log('no pin');
                                    app.views.main.router.navigate('/pin/');
                                } else {
                                    app.views.main.router.navigate('/dashboard/');
                                    getPlatforms();
                                    getConfig();
                                }
                            }
                        }
                    }
                    // Close login screen
                    //app.loginScreen.close('#my-login-screen');
                } else {
                    toast(data.FailureReason);
                }
                */
            },
            error: function(xhr, textStatus, errorThrown) {
                console.log(xhr)
                console.log(textStatus)
                app.preloader.hide();
                toast(errorThrown);
                //app.dialog.alert('Error.');
            }
    });
}

// Cancel order only for orders with shipment
function cancelOrder(orderId, email){
    app.preloader.show();
    $.ajax({
        type: 'GET',
        url: api + 'shipment/cancel',
        cache: false,
        crossDomain: true,
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        processData: true,
        data: {
            orderId: orderId
        },
        headers: {
            "ApiToken": "nvt.api."+localStorage.getItem('Token'),
            "DeveloperID": devId,
            "ContextID": localStorage.getItem('ContextID')
        },
        success: function(data, status) {
            console.log('update order btn - cancelOrder')
            console.log(data);
            app.preloader.hide();
            if (data === 'Success') {
                updateContactDetails(orderId, email);
            }
        },
        error: function(xhr, textStatus, errorThrown) {
            app.preloader.hide();
            toast(errorThrown);
            //app.dialog.alert('Error.');
        }
    });
}

function updateContactDetails(orderId, email){
    console.log('updateContactDetails '+orderId+' and '+email);
    app.preloader.show();
    var deliveryTo = {
        "FullName": $('#order .name').val(),
        "CompanyName": $('#order .company').val(),
        "AddressLine1": $('#order .address1').val(),
        "AddressLine2": $('#order .address2').val(),
        "City": $('#order .city').val(),
        "County": $('#order .county').val(),
        "Postcode": $('#order .postcode').val(),
        "Country":  $('#order .country').val(),
        "TelephoneNumber": $('#order .phone').val(),
        "Fax": $('#order .fax').val()
    };
    $.ajax({
        type: 'POST',
        url: api + 'order/UpdateContactDetails',
        cache: false,
        crossDomain: true,
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        processData: true,
        data:{
            OrderID: orderId,
            DeliveryTo: deliveryTo,
            InvoiceTo: deliveryTo,
            EmailAddress: email
        },
        headers: {
            "ApiToken": "nvt.api."+localStorage.getItem('Token'),
            "DeveloperID": devId,
            "ContextID": localStorage.getItem('ContextID')
        },
        success: function(data, status) {
            console.log(data);
            if(data === 'Success') {
                toast("Order updated!");
            } else {
                toast("Error updating order.");
            }
            app.preloader.hide();
        },
        error: function(error) {
            app.preloader.hide();
            toast(error);
        }
    });
}

function getCouriers(){
    //app.preloader.show();
    $.ajax({
        type: 'GET',
        url: api + 'shipment/getCouriers',
        cache: false,
        crossDomain: true,
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        processData: true,
        headers:{
            "ApiToken": "nvt.api."+localStorage.getItem('Token'),
            "DeveloperID": devId,
            "ContextID": localStorage.getItem('ContextID')
        },
        success: function(data) {
            //console.log('getCouriers');
            //console.log(data);
            //app.preloader.hide();
            //var couriers = JSON.stringify(data);
            var couriers = []
            $.each(data, function(i,v){
                couriers.push({
                    ID: v.ID,
                    Name: v.Name,
                    Pieces: v.AdHocParams.Pieces,
                    Preprinted: v.IsPrePrinted //if true > TrackingNumber is required
                });
            });
            //console.log(couriers);
            localStorage.setItem('couriers', JSON.stringify(couriers));
        },
        error: function(xhr, ajaxOptions, thrownError) {
            //app.preloader.hide();
            if(xhr.status == 403) {
                var curPage = app.views.main.router.currentPageEl;
                if($(curPage).data('name') !== 'lockscreen'){
                    toast('Previous user has been logged out, please log in with your PIN.');
                    app.views.main.router.navigate('/lockscreen/');
                }
                return;
            }
            app.dialog.alert(thrownError);
        }
    });
}

var waitToGetConfig = true;

function getConfig(){
    app.preloader.show();
    waitToGetConfig = true;
    $.ajax({
        type: 'GET',
        url: api + 'shipment/getConfig',
        //cache: false,
        crossDomain: true,
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        processData: true,
        headers:{
            "ApiToken": "nvt.api."+localStorage.getItem('Token'),
            "DeveloperID": devId,
            "ContextID": localStorage.getItem('ContextID')
        },
        success: function(data, status) {
            //console.log('getConfig');
            //console.log(data);
            app.preloader.hide();
			var contextId = localStorage.getItem('ContextID');
			var shipmentConfigKey = "ShipmentConfig-"+contextId;
			localStorage.setItem(shipmentConfigKey, JSON.stringify(data));
			waitToGetConfig = false;
			$('#tastatura').animate({ opacity: 1 }, 100);
        },
        error: function(xhr, textStatus, errorThrown) {
            app.preloader.hide();
            if(xhr.status == 403){
                var curPage = app.views.main.router.currentPageEl;
                if($(curPage).data('name') !== 'lockscreen'){
                    toast('Previous user has been logged out, please log in with your PIN.');
                    app.views.main.router.navigate('/lockscreen/');
                }
                return;
            }
            if(xhr.status == 500){
				waitToGetConfig = false;
				$('#tastatura').animate({ opacity: 1 }, 100);
				localStorage.getItem('StoreName')
				toast(localStorage.getItem('StoreName')+' doesnt have config!');
				return;
            }
            app.dialog.alert(errorThrown);
        }
    });
}

/*
function storeSelect(){
    var allContextIds = localStorage.getItem('allContextIds');
    if(allContextIds !== null && allContextIds) {
        if(!$('#change-store').val() ) {
            var sel = '';
            $.each(JSON.parse(allContextIds), function(i,v){
                sel += '<option value="'+v.Id+'">'+v.Name+'</option>';
            });
            $('#change-store').append(sel);
        }
    }                
    var storeSelect = app.smartSelect.create({
        el: '.store-select',
        openIn: 'popup',
        popupCloseLinkText: 'Select',
        popupTabletFullscreen:true,
        closeOnSelect: false,
        routableModals: false,
        searchbar: true,
        pageTitle: 'Select Store',
        on: {
            close:function(el){
                //app.preloader.show();
                //console.log($('#change-store').val());
                localStorage.setItem('StoreName', $(storeSelect.valueEl).text());
                localStorage.setItem('ContextID', storeSelect.getValue()); //cId
                //localStorage.setItem('Token', data.Token.replace('nvt.api.',''));
                $('#myName').html(localStorage.getItem('StoreName')+'<br>'+localStorage.getItem('FirstName') + ' ' + localStorage.getItem('LastName'));
                app.views.main.router.navigate('/dashboard/');
                $('input.keypad').val('');
                getPlatforms();
                getCouriers();
                getConfig();
                getToday();
            }
        }
    });
    storeSelect.open();
}
*/

function signinbyPIN(cId, pin, callback){
	app.preloader.show();
    //console.log('signinbyPIN '+pin+'\n'+cId);
    var phone;
    if(!app.device.desktop) {
        phone = {
            uuid: device.uuid,
            manufacturer: device.manufacturer,
            model: device.model
        }
        console.log(phone);
    }
    console.log(phone);
    $.ajax({
        type: 'post',
        url: api + 'auth/signinbypin',
        crossDomain: true,
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        processData: true,
        data: {
            "Pin": pin,
            "Device": phone,
        	"ApplicationID": appId,
            "DeveloperID": devId,
            "ContextID": cId
        },
        success: function(data) {
            //console.log(data);
            app.preloader.hide();
            if(data.Success){
                if(data.Contexts[0].DisabledReason.length){
                    app.dialog.alert(data.Contexts[0].DisabledDate+' - '+data.Contexts[0].DisabledReason);
                    logout();
                    return;
                }
                callback(data);
                return;
            }
        	//if(data.Success) {
                //callback(data);
        		//localStorage.setItem('UserId', data.UserId)
        		//localStorage.setItem('Token', data.Token)
        		//localStorage.setItem('FirstName', data.FirstName)
        		//localStorage.setItem('LastName', data.LastName)
        	
        		//var contexts = JSON.stringify(data.Contexts)
                //localStorage.setItem('Contexts', contexts)
                //return;
        	//} 
            //toast(data.FailureReason);
            app.dialog.alert(data.FailureReason, function () {
                app.views.main.router.navigate(app.views.main.router.currentRoute.url, {
                    ignoreCache: true,
                    reloadCurrent: true
                });
            });
        },
        error: function(error){
        	app.preloader.hide();
        	console.log(error);
        }
    });
}

function setUserPIN(pin, callback){
	app.preloader.show();
	console.log('setUserPIN '+pin);
    $.ajax({
        type: 'POST',
        url: api + 'store/SetUserPin?userPin='+pin,
        crossDomain: true,
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        processData: true,
        headers: {
            "ApiToken": "nvt.api."+localStorage.getItem('Token'),
            "DeveloperID": devId,
            "ContextID": localStorage.getItem('ContextID')
        },
        success: function(data) {
        	app.preloader.hide();
        	if(data.statusText === "OK") {
                toast('Your new Pin Code is <b>' + pin + '</b>');
            } else {
                toast('Please try another PIN code.');
            }
            console.log(data.statusText);
            callback(data);
        },
        error: function(error){
        	app.preloader.hide();
            var e = error.statusText;
        	if(e === 'OK') {
                toast('Your new Pin Code is <b>' + pin + '</b>');
                app.views.main.router.navigate('/dashboard/');
            } else if(e === 'Internal Server Error') {
            	toast('Please try another PIN code.');
            } else {
                toast(e);
            }
        	console.log(error);
            callback(e);
        }
    });
}

function getToday(){
    var today = new Date(); 
    var dd = today.getDate(); 
    var mm = today.getMonth() + 1; 
    var yyyy = today.getFullYear(); 
    if (dd < 10) dd = '0' + dd; 
    if (mm < 10) mm = '0' + mm; 
    var today = yyyy+'-'+mm+'-'+dd; 
    //console.log(today);
    //getOrders(today, 0);
    getOrders();
    //getCouriers();
}

function pad(str, max){
  	str = str.toString();
  	return str.length < max ? pad("0" + str, max) : str;
}

function searchForOrder(id){
    id = id.replace(/\D/g, '');
    if(waitToGetConfig) {
    	toast('Courier data is downloading, please try again.');
    	$('input.keypad, #order input[type="text"], #order input[type="tel"]').val('');
    	return;
    }
    app.preloader.show();
    getCouriers();
    $.ajax({
        type: 'GET',
        url: api + 'order',
        timeout: 10000, // 10 seconds
        crossDomain: true,
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        processData: true,
        data: {
            QueryType: 'OrderNumber',
            Query:id
        },
        headers: {
            "ApiToken": "nvt.api."+localStorage.getItem('Token'),
            "DeveloperID": devId,
            "ContextID": localStorage.getItem('ContextID')
        },
        success: function(data, status) {
        	//console.log('searchForOrder '+id);
            data = JSON.parse(data);
            console.log(data);

			var theRealShippingService = getRealShippingService(data.Details.Items[0].DeliveryService,data.Details.Items[0].DeliveryMethod);

			var defaultShipmentServiceName = null;
			if (theRealShippingService) {
                defaultShipmentServiceName = getShippingServiceDesription(theRealShippingService);
                console.log('theRealShippingService / defaultShipmentServiceName: '+defaultShipmentServiceName);
			}

			var couriers = JSON.parse(localStorage.getItem('couriers'));
			var listOfCourriers = [];
			if (defaultShipmentServiceName) {
				listOfCourriers.push({
	                ID: null, 
	                Name: defaultShipmentServiceName
	            });
				//ID with null is default
			}
			
			if (couriers && couriers.length > 0) {
				listOfCourriers = listOfCourriers.concat(couriers);
			}

            var orderShipments = [];
            data.Shipments.forEach(function(shipment){
                var parcels = [];
                //console.log('--- shipment')
                //console.log(shipment)
            	shipment.Parcels.forEach(function(parcel){
            		parcels.push({
            			UniqueRef: parcel.UniqueRef
            		});
            	});
            	var obj = {
            		ID: shipment.ID,
            		Parcels: parcels,
            		Description: getShipmentDescription(shipment),
                    ApiCourierId: (shipment.Attributes && shipment.Attributes["ApiCourierId"] ? shipment.Attributes["ApiCourierId"] : null)
            	};
            	if(shipment.Attributes.AdditionalShipmentCreationReason){
            		obj.AdditionalShipmentCreationReason = shipment.Attributes.AdditionalShipmentCreationReason;
            	}
            	orderShipments.push(obj);
            });

            //console.log('--- orderShipments');
            //console.log(orderShipments);

            app.preloader.hide();
            var shipmentRef = data.Shipment.UniqueRef;
            var attributes = data.Shipment.Attributes;
            if(attributes){
                var courier = attributes.ProviderType;
            }
            data = data.Details;

            var status = data.Status;
            var ord = data.OrderNumber;
            var items = data.Items.length;
            	items = items + (items > 1 ? ' Items' : ' Item');
            var products = data.Items;
            var total = data.Total.Gross;

            var currency = "";
            switch(total.Currency){
				case "GBP": 
					currency = "&pound;";
					break;
				case "USD":
					currency = "&dollar;";
					break;
				case "EUR": 
					currency = "&euro;";
					break;
				default:
					currency = total.Currency;
			}

            total = currency+total.Value;
            var delivery = data.DeliveryTo;
            var country = delivery.Country;
            var email = data.EmailAddress;

            if(ord.replace(/\D/g, '') === id){
                //console.log('order found, add/update dashboard order '+id+' - status: '+status);
                //$('input.keypad').val('');
                $('input.keypad, #order input[type="text"], #order input[type="tel"]').val('');
                $('#order button').removeClass('color-green');
                $('#despatched').hide();
                $('#order').attr({
                    'data-id': data.ID,
                    'data-number': id
                }).show();
                $('.page[data-name="dashboard"] > .navbar .title').html(ord+' - <small style="font-size:16px">'+status+'</small>');

                var m = new Date(data.OrderDate);
                var dateString = m.getDate() +"/"+ (m.getMonth()+1) +"/"+ m.getFullYear() + " at " + m.getHours() + ":" + m.getMinutes() // m.getUTCSeconds();
                $('.page[data-name="dashboard"] > .navbar .right').html('<p>Total: '+total+'  Date: '+dateString+'</p>');

                $('#order .products').html('').show();
                var prod = '<ul class="product">';
                $.each(products, function(i,v){
                    if(v.ImageUrl !== ''){
                        prod += '<li class="thumb">';
                        prod += '<img src="'+v.ImageUrl+'" data-title="'+v.Title+'" class="thumb-popup popup-open" data-popup="#thumb-popup" />';
                        prod += '<span style="';
                        if(v.Quantity > 1) {
                            prod += 'background:#FF3B30; color:#fff;';
                        } else {
                            prod += 'border:1px #FF3B30 solid; background:#fff; color:#444;';
                        }
                        prod += '">'+v.Quantity+'</span>';
                        prod += '</li>';
                    }
                });
                prod += '</ul>';
                $('#order .products').html(prod);

                if($('#order .products ul li').length >= 1){
                    $('.thumb-popup').on('click', function () {
                        var title = $(this).data('title');
                        var img = $(this).attr('src');
                        $('#thumb-popup .title').text(title);
                        $('#thumb-popup .page-content').html('<img src="'+img+'" />');
                    });
                } else {
                	$('#order .products').hide();
                }

                $('#order .total').html(items);
                $('#order .name').val(delivery.FullName);
                $('#order .phone').val(delivery.TelephoneNumber);
                $('#order .fax').val(delivery.Fax);
                $('#order .address1').val(delivery.AddressLine1);
                $('#order .address2').val(delivery.AddressLine2);
                $('#order .company').val(delivery.CompanyName);
                $('#order .city').val(delivery.City);
                $('#order .county').val(delivery.County);
                $('#order .country option:selected').removeAttr("selected");
                $('#order .country option[value="'+country+'"]').attr('selected', 'selected');
                $('#order .postcode').val(delivery.Postcode);
                
                $('.label-button').html('');

                var html = '';
                if(theRealShippingService){
                    //console.log(theRealShippingService);
                    var buttonId = "";
                    var UniqueRef = "";
                    if(orderShipments.length > 0){
                        $.each(orderShipments, function(i,v){
                            UniqueRef = v.Parcels[0].UniqueRef;
                            buttonId = v.ID
                        });
                    }
                    html += '<button data-id="'+buttonId+'" data-uniqueref="'+UniqueRef+'" disabled ';
                    html += 'class="col text-color-blue text-align-left" style="line-height:34px; background:transparent; border:0; font-size:16px; font-weight:700; padding-left:0">';
                    html += theRealShippingService.AppDefaultShipment+'</button>';
                } else {
                    //console.log(data.Items[0]);
                    html += '<button disabled ';
                    html += 'class="col text-color-blue text-align-left" style="line-height:34px; background:transparent; border:0; font-size:16px; font-weight:700; padding-left:0">';
                    html += data.Items[0].DeliveryService+'</button>';
                }

                if(orderShipments.length > 0){
                    //console.log(listOfCourriers);
                	$.each(listOfCourriers, function(i,v){
                        html += '<button data-id="'+v.ID+'" ';
                        if(v.Preprinted) {
                            html += 'onclick="scanLabel(\''+data.ID+'\', \''+ord+'\', \''+v.ID+'\')" title="scan label" ';
                        } else {
                            html += 'onclick="createAdhocShipmentPrintAndDespatch(\''+data.ID+'\', \''+ord+'\', \''+v.ID+'\')" title="create adhoc shipment and despatch" ';
                        }
                        html += 'class="col button button-fill button-raised '+(!v.ID ? 'color-green' : 'color-gray')+'">';
                        html += v.Name+'</button>';
                	});
                } else {
                    $.each(orderShipments, function(i,v){
                        if(v.ApiCourierId !== null ){
                            if(v.AdditionalShipmentCreationReason == undefined){
                                html += '<button data-id="'+v.ID+'" data-uniqueref="'+v.Parcels[0].UniqueRef+'" class="col button button-fill button-raised color-green" ';
                                html += 'onclick="getLabelId(\''+v.ID+'\', \''+v.Parcels[0].UniqueRef+'\')" title="get label">';
                                if(defaultShipmentServiceName == null){
                                    html += v.Description;
                                } else {
                                    html += defaultShipmentServiceName;
                                }
                                html += '</button>';
                            }
                        } else {
                            console.log('4');
                            console.log(v);
                        }
                    });
                    //console.log(listOfCourriers);
                	$.each(listOfCourriers, function(i,v){
                        html += '<button data-id="'+v.ID+'" ';
                        if(v.Preprinted) {
                            html += 'onclick="scanLabel(\''+data.ID+'\', \''+ord+'\', \''+v.ID+'\')" title="scan label" ';
                        } else {
                            html += 'onclick="createShipmentPrintAndDespatch(\''+data.ID+'\', \''+ord+'\', \''+v.ID+'\')" title="create shipment and despatch" ';
                        }
                        html += 'class="col button button-fill button-raised '+(!v.ID ? 'color-green' : 'color-gray')+'">';
                        html += v.Name+'</button>';
                	});
            	}

                $('.label-button').append(html);
	            if(orderShipments.length > 0){
	                $(".label-button > button[data-id='null']").remove();
	            }
	            var btns = $(".label-button > button");
	            for(var i = 0; i < btns.length; i+=2) {
	                btns.slice(i, i+2).wrapAll("<div class='row'></div>");
                }
                /*
	            $(".label-button .row").each(function(){
	                var childs = $(this).children().length;
	                if(childs < 2){
	                    $(this).children().wrapAll('<div class="col-50"></div>');
	                }
                });
                */

                $('.actions').html("");
                var actions = "";
                //if(orderShipments.length > 0){
                actions += '<button class="action-button col button button-fill button-raised color-red" onclick="cancelShipment()">Cancel order shipment</button>';
                $('#order').attr({
                    'data-shipmentRef': shipmentRef,
                    'data-ord': ord
                });
                //}
                $('.actions').append(actions);

                var orderInputs = $('#order input[type="text"], #order input[type="tel"], #order select');
                orderInputs.prop("disabled", true);

                $('#edit-address').unbind().on('click', function(e) {
                    e.preventDefault();
                    var orderInputs = $('#order input[type="text"], #order input[type="tel"], #order select');
                    if ($(this).hasClass('color-blue')) {
                        orderInputs.prop("disabled", false);
                        $(this).removeClass('color-blue').addClass('color-orange').text('Save Address');
                        $('.actions, .label-button').hide();
                    } else {
                        orderInputs.removeClass('input-with-value').prop("disabled", true);
                        $(this).removeClass('color-orange').addClass('color-blue').text('Edit Address');
                        $('#order li.item-input').removeClass('item-input-with-value');
                        $('.actions, .label-button').show();
                        updateContactDetails(data.ID, localStorage.getItem('username'));
                    }
                });
            } else {
                toast('Order not found!');
                $('#order').removeAttr('id').removeAttr('number').hide();
            }
        },
        error: function(xhr, textstatus, message) {
            app.preloader.hide();
            if(xhr.status == 403){
                var curPage = app.views.main.router.currentPageEl;
                if($(curPage).data('name') !== 'lockscreen'){
                    toast('Previous user has been logged out, please log in with your PIN.');
                    app.views.main.router.navigate('/lockscreen/');
                }
                return;
            }
            if(textstatus === "timeout") {
                toast("Got timeout. Please try again.");
                return;
            } 
            console.log(textstatus);
            toast('Order not found. Please try again.');
            $('input.keypad').val('');
        }
    });   
}

function forgotPassword(){
    if (!checkInputs('.retrieve-email', '.retrieve-button')) {
    	toast('Please enter a valid email address.');
    	return;
    }
    $.ajax({
        type: 'GET',
        url: 'https://app.nventree.com/api/account/sendresetpasswordlink',
        cache: false,
        crossDomain: true,
        data: {
        	userNameOrEmail: $('.retrieve-email').val()
        },
        success: function(data, status) {
        	//console.log(data);
        	if(data === "Ok") {
        		toast('Email with password sent to '+$('.retrieve-email').val());
        	}
        },
        error: function(xhr, textStatus, errorThrown) {
        	console.log(xhr);
        	console.log(textStatus);
        	toast('Something went wrong, please try again.');
        }
	});
}

/*
function addParcel(){
	$.ajax({
        type: 'POST',
        url: api + 'shipment/addParcel',
		crossDomain: true,
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        processData: true,
        headers: {
            "ApiToken": "nvt.api."+localStorage.getItem('Token'),
            "DeveloperID": devId,
            "ContextID": localStorage.getItem('ContextID')
        },
        success: function(data, status) {
            //console.log('addParcel');
            //console.log(data);
        },
        error: function(error){
        	console.log(error);
        }
    });
}

function cancelParcel(OrderId, UniqueRef){
	$.ajax({
        type: 'POST',
        url: api + 'shipment/cancelParcel',
		crossDomain: true,
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        processData: true,
        data :{
            OrderId: OrderId,
            UniqueRef: UniqueRef,
            Reason: 'Parcel canceled from mobile app'
        }
        headers: {
            "ApiToken": "nvt.api."+localStorage.getItem('Token'),
            "DeveloperID": devId,
            "ContextID": localStorage.getItem('ContextID')
        },
        success: function(data, status) {
            console.log('cancelParcel');
            console.log(data);
        },
        error: function(error){
        	console.log(error);
        }
    });
}
*/

function markAsDespatched(refresh){
    var orderId = $('#order').attr('data-id');
    app.preloader.show();
    $.ajax({
        type: 'GET',
        url: api + 'orders/MarkAsDespatched?orderId='+orderId,
        crossDomain: true,
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        processData: true,
        headers: {
            "ApiToken": "nvt.api."+localStorage.getItem('Token'),
            "DeveloperID": devId,
            "ContextID": localStorage.getItem('ContextID')
        },
        success: function(data, status) {
            //console.log(data);
            if(data === "Success") {
            	toast('Order despatched'); 
            	//console.log(refresh);
            	if(refresh) {
            		var ord = $('#order').attr('data-number');
                    searchForOrder(ord);
                    return;
            	}
                $('.page[data-name="dashboard"] .navbar .title small').text('Despatched');
            }
            app.preloader.hide();
        },
        error: function(error){
            app.preloader.hide();
            console.log(error);
            toast(error.responseJSON.Message);
        }
    });
}

function clearDashboard(){
    getToday();
    $('input.keypad, #order input').val('');
    $('.page[data-name="dashboard"] > .navbar .title').html('Dashboard');
    $('.page[data-name="dashboard"] > .navbar .right').html('');
    $('#order').hide();
    $('#despatched').show();
    $('input.keypad').focus();
    window.setTimeout(function(){
        if (window.cordova) {
            Keyboard.hide();
        }
    }, 1);
 }

function cancelShipment(){
    var orderId = $('#order').attr('data-id');
    var id = $('#order').attr('data-number');
    //id = id.replace(/\D/g, '');
    app.preloader.show();
    getCouriers();
    $.ajax({
        type: 'GET',
        url: api + 'order',
        timeout: 10000, // 10 seconds
        crossDomain: true,
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        processData: true,
        data: {
            QueryType: 'OrderNumber',
            Query:id
        },
        headers: {
            "ApiToken": "nvt.api."+localStorage.getItem('Token'),
            "DeveloperID": devId,
            "ContextID": localStorage.getItem('ContextID')
        },
        success: function(data, status) {
            app.preloader.hide();
        	//console.log('searchForOrder '+id);
            data = JSON.parse(data);
            //console.log(data);

            var values = [];
            var displayValues = [];

            data.Shipments.forEach(function(shipment){
                var uniqueref = shipment.Parcels[0].UniqueRef;
                var name = ""
                if(shipment.Attributes.ProviderType){
                    name += shipment.Attributes.ProviderType
                }
                if(shipment.Attributes.Code){
                    name += ' - '+shipment.Attributes.Code
                }
                if(uniqueref !== undefined) {
                    name = name+' ('+uniqueref+')';
                }
                values.push(shipment.ID);
                displayValues.push(name);
            });
            if(values.length == 0){
                toast('Order has no shipments.');
                return;
            }
            var picker = app.picker.create({
                inputEl: '#pickShipmentToCancel',
                openIn: 'popover',
                renderToolbar: function () {
                    return '<div class="toolbar">' +
                      '<div class="toolbar-inner">' +
                        '<div class="left">'+
                            '<a href="#" class="link button button-fill color-red sheet-close popover-close cancel-selected" style="height:30px; margin-left:6px">CANCEL SHIPMENT</a>'+
                        '</div>' +
                        '<div class="right">' +
                          '<a href="#" class="link sheet-close popover-close"><i class="icon f7-icons">xmark_circle</i></a>' +
                        '</div>' +
                      '</div>' +
                    '</div>';
                },
                on: {
                    open: function (picker) {
                        $('.cancel-selected').on('click', function(e) {
                            var shipmentId = picker.getValue();
                            console.log('cancelShipment orderId '+orderId+' shipmentId '+shipmentId);
                            app.preloader.show();
                            $.ajax({
                                type: 'GET',
                                url: api + 'shipment/CancelShipmentById?orderId='+orderId+'&shipmentId='+shipmentId,
                                crossDomain: true,
                                contentType: "application/x-www-form-urlencoded",
                                dataType: "json",
                                processData: true,
                                headers: {
                                    "ApiToken": "nvt.api."+localStorage.getItem('Token'),
                                    "DeveloperID": devId,
                                    "ContextID": localStorage.getItem('ContextID')
                                },
                                success: function(data, status) {
                                    console.log(data);
                                    app.preloader.hide();
                                    if(data === "Success") {
                                        var ord = $('#order').attr('data-number');
                                        searchForOrder(ord);
                                    }
                                    toast(data);
                                },
                                error: function(error){
                                    app.preloader.hide();
                                    console.log(error);
                                }
                            });
                        });
                    }
                },
                cols: [
                    {
                        values: values,
                        displayValues: displayValues,
                    }
                ]
            }).open();
        }
    });
}

function createAdhocShipmentPrintAndDespatch(orderId, orderNr, curierId){
    var labelStepperMax = 10;
    var btn = $('.label-button button[data-id="'+curierId+'"]');
    var btn_text = btn.text().toLowerCase();
    if (btn_text.match( /(hermes|bagit)/ ) || btn.hasClass('color-green')) {
        labelStepperMax = 1;
    }
    var printer = localStorage.getItem('defaultPrinter');
    var ip = localStorage.getItem('defaultIP');
    if(printer == 'null'){
        app.dialog.confirm('Please select a default printer in Settings.', function () {
            app.views.main.router.navigate('/settings/');
        });
        return;
    }
    if(ip == 'null'){
        app.dialog.confirm('Please add printer IP in Settings.', function () {
            app.views.main.router.navigate('/settings/');
        });
        return;
    }
    var dialog = app.dialog.create({
        title: '<small class="text-color-blue">'+btn.text()+'</small><br>Create Labels',
        animate: false,
        content: `<div style="display:block; width:100%; padding-top:30px; text-align:center">
                    <div id="labelCount" class="stepper stepper-raised stepper-fill">
                        <div class="stepper-button-minus"></div>
                        <div class="stepper-input-wrap" style="background: #fff">  
                            <input type="text" value="1" min="1" max="4" step="1" readonly>
                        </div>
                        <div class="stepper-button-plus"></div>
                    </div>
                </div>`,
        on: {
            opened: function () {
                labelCount = app.stepper.create({
                    el: '#labelCount',
                    max: labelStepperMax
                });
            },
            open: function (e) {   
                //console.log('Dialog open');
                $(e.$el).find('.dialog-title').css({'text-align':'center'});
                $(e.$el).find('.dialog-buttons').css({'justify-content':'center', 'padding-bottom':"24px", 'height':"auto"});
                $(e.$el).find('.dialog-buttons span').addClass('button color-green button-fill').css({'color':'#fff', 'margin':'0 10px','min-width':'auto','padding':'0 20px'});
                $(e.$el).find('.dialog-buttons span:first-child').removeClass('color-green').addClass('color-red');
            }
        },
        buttons: [{
            text:'Cancel',
            close:true
        }, {
            text:'Print',
            onClick: function(dialog, e){
                app.preloader.show();
                var shipmentData = {
                    ApiCourierId: curierId,
                    CreationReason: "Nventree Pick and Pack: Aditional Shipment"
                }
                //if(trackingNr){
                    //shipmentData.TrackingNumber = trackingNr;
                //}
                if (btn_text.match( /(hermes|bagit)/ ) || btn.hasClass('color-green')) {
                    shipmentData.Pieces = labelStepperMax;
                } else {
                    shipmentData.Pieces = labelCount.getValue();
                }
                $.ajax({
                    type: 'POST',
                    url: api + 'shipment/CreateAdditionalShipment?orderId='+orderId,
                    crossDomain: true,
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    processData: true,
                    data: shipmentData,
                    headers: {
                        "ApiToken": "nvt.api."+localStorage.getItem('Token'),
                        "DeveloperID": devId,
                        "ContextID": localStorage.getItem('ContextID')
                    },
                    success: function(data, status) {
                        console.log(data);
                        if(data.Result.CountOfSuccessful === 0){
                            app.preloader.hide();
                            app.dialog.alert(data.Result.ErrorList[0].ErrorMessage);
                            searchForOrder(orderNr.replace(/\D/g, ''));
                            return;
                        }
                        //btn.removeClass('color-gray').addClass('color-green');
                        //btn.attr("onclick", "alert('Order Despatched')");
                        $.ajax({
                            type: 'POST',
                            url: api + 'print/getLabels',
                            crossDomain: true,
                            contentType: "application/x-www-form-urlencoded",
                            dataType: "json",
                            processData: true,
                            headers: {
                                "ApiToken": "nvt.api."+localStorage.getItem('Token'),
                                "DeveloperID": devId,
                                "ContextID": localStorage.getItem('ContextID')
                            },
                            data: {
                                ApplicationID: appId,
                                OrderIDs: [orderId]
                            },
                            success: function(data) {
                                //console.log('print/getLabels - order id '+orderId);
                                //console.log(data);
                                if(data.length){
									$.get(data, function(content) {
                                        if(content == '') {
                                            app.preloader.hide();
                                            toast('Something is wrong with the label. Please try again.');
                                            return;
                                        }
                                        //console.log(content);
										var labels = $(content).map(function(){
                                            var courier = $(this).attr('data-courier');
											if(this.tagName !== 'BR' && courier == curierId) {
                                                //console.log(this);
                                                return this.src.replace('data:image/png;base64,','');
                                            }
                                        }).get();
                                        //console.log(labels);
                                        //console.log(shipmentData.Pieces)'=;
                                        if(labels.length > 1){
                                            labels = labels.slice(-shipmentData.Pieces);
                                        }
										$.each(labels, function(i, base64Img){
                                            //console.log('createImage for ordrer '+orderNr+' - '+curierId);
                                            if(!app.device.desktop) {
                                                createImage(base64Img, orderNr+'-'+i);
                                            }
										});
									}).done(function() {
                                        markAsDespatched(true);
									});
                                } else {
                                    toast('No label.');
                                }
                            },
                            error: function(xhr, textStatus, errorThrown) {
                                app.preloader.hide();
                                toast(errorThrown);
                            }
                        });
                    },
                    error: function(error){
                        app.preloader.hide();
                        console.log(error);
                    }
                });
            },
        }]
    }).open();
}

function createShipmentPrintAndDespatch(orderId, orderNr, curierId){ //trackingNr
    var labelStepperMax = 10;
    var btn = $('.label-button button[data-id="'+curierId+'"]');
    var btn_text = btn.text().toLowerCase();
    if (btn_text.match( /(hermes|bagit)/) || btn.hasClass('color-green')) {
        labelStepperMax = 1;
    } 
    var printer = localStorage.getItem('defaultPrinter');
    var ip = localStorage.getItem('defaultIP');
    if(printer == 'null'){
        app.dialog.confirm('Please select a default printer in Settings.', function () {
            app.views.main.router.navigate('/settings/');
        });
        return;
    }
    if(ip == 'null'){
        app.dialog.confirm('Please add printer IP in Settings.', function () {
            app.views.main.router.navigate('/settings/');
        });
        return;
    }
    //console.log('createShipmentPrintAndDespatch');
    var dialog = app.dialog.create({
        title: '<small class="text-color-blue">'+btn.text()+'</small><br>Print Label',
        animate: false,
        content: `<div style="display:block; width:100%; padding-top:30px; text-align:center">
                    <div id="labelCount" class="stepper stepper-raised stepper-fill">
                        <div class="stepper-button-minus"></div>
                        <div class="stepper-input-wrap" style="background: #fff">  
                            <input type="text" value="1" min="1" max="4" step="1" readonly>
                        </div>
                        <div class="stepper-button-plus"></div>
                    </div>
                </div>`,
        on: {
            opened: function () {
                labelCount = app.stepper.create({
                    el: '#labelCount',
                    max: labelStepperMax
                });
            },
            open: function (e) {   
                $(e.$el).find('.dialog-title').css({'text-align':'center'});
                $(e.$el).find('.dialog-buttons').css({'justify-content':'center', 'padding-bottom':"24px", 'height':"auto"});
                $(e.$el).find('.dialog-buttons span').addClass('button color-green button-fill').css({'color':'#fff', 'margin':'0 10px','min-width':'auto','padding':'0 20px'});
                $(e.$el).find('.dialog-buttons span:first-child').removeClass('color-green').addClass('color-red');
            }
        },
        buttons: [{
            text:'Cancel',
            close:true
        }, {
            text:'Print',
            onClick: function(dialog, e){
                app.preloader.show();
                var shipmentData =  {
                    ApiCourierId: curierId,
                    Pieces: labelCount.getValue()
                }
                //if(trackingNr){
                    //shipmentData.TrackingNumber = trackingNr;
                //}
                if (btn_text.match( /(hermes|bagit)/ )) {
                    shipmentData.Pieces = labelStepperMax;
                } else {
                    shipmentData.Pieces = labelCount.getValue();
                }
                console.log(shipmentData);
                $.ajax({
                    type: 'POST',
                    url: api + 'shipment/Create?orderId='+orderId,
                    crossDomain: true,
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    processData: true,
                    data: shipmentData,
                    headers: {
                        "ApiToken": "nvt.api."+localStorage.getItem('Token'),
                        "DeveloperID": devId,
                        "ContextID": localStorage.getItem('ContextID')
                    },
                    success: function(data, status) {
                        //console.log(data);
                        if(data.Result.CountOfSuccessful === 0){
                            app.preloader.hide();
                            app.dialog.alert(data.Result.ErrorList[0].ErrorMessage);
                            searchForOrder(orderNr.replace(/\D/g, ''));
                            return;
                        }
                        //btn.removeClass('color-gray').addClass('color-green');
                        //btn.attr("onclick", "alert('Order Despatched')");
                        $.ajax({
                            type: 'POST',
                            url: api + 'print/getLabels',
                            crossDomain: true,
                            contentType: "application/x-www-form-urlencoded",
                            dataType: "json",
                            processData: true,
                            headers: {
                                "ApiToken": "nvt.api."+localStorage.getItem('Token'),
                                "DeveloperID": devId,
                                "ContextID": localStorage.getItem('ContextID')
                            },
                            data: {
                                ApplicationID: appId,
                                OrderIDs:[orderId]
                            },
                            success: function(data) {
                                //console.log('print/getLabels - order id '+orderId);
                                //console.log(data);
                                if(data.length){
									$.get(data, function(content) {
										if(content == '') {
                                            app.preloader.hide();
                                            toast('Something is wrong with the label. Please try again.');
                                            return;
                                        }
                                        console.log(content);
										var labels = $(content).map(function(){
                                            console.log($(this))
                                            if(this.tagName !== 'BR'){
											    if(!$(this).attr('data-courier')){
                                                    return this.src.replace('data:image/png;base64,','');
                                                } else {
                                                    if(curierId == $(this).attr('data-courier')){
                                                        return this.src.replace('data:image/png;base64,','');
                                                    }
                                                }
                                            }
                                        }).get();
                                        //console.log(labels.length);
                                        if(labels.length > 1) {
                                            labels = labels.slice(-shipmentData.Pieces);
                                        }
                                        //console.log(labels);
                                        $.each(labels, function(i, base64Img){
                                            console.log('createImage '+orderNr+'-'+i);
                                            if(!app.device.desktop) {
                                                createImage(base64Img, orderNr+'-'+i); //OrderNumber
                                            }
										});
									}).done(function() {
                                        markAsDespatched(true);
									});
                                } else {
                                    toast('No label.');n
                                }
                            },
                            error: function(xhr, textStatus, errorThrown) {
                                app.preloader.hide();
                                toast(errorThrown);
                            }
                        });
                    },
                    error: function(error){
                        app.preloader.hide();
                        console.log(error);
                    }
                });
            },
        }]
    }).open();
}

//var labelPieces;
function createShipment(orderId, orderNr, curierId, trackingNr, despatched){
    //console.log('shipment/Create?orderId='+orderId+' orderNr '+orderNr+' curierId '+curierId+' trackingNr: '+trackingNr)
    var shipmentData =  {
        ApiCourierId: curierId,
        Pieces: 1
    }
    if(trackingNr){
        shipmentData.TrackingNumber = trackingNr;
    }
    //console.log(data);
    app.preloader.show();
    $.ajax({
        type: 'POST',
        url: api + 'shipment/Create?orderId='+orderId,
        crossDomain: true,
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        processData: true,
        data: shipmentData,
        headers: {
            "ApiToken": "nvt.api."+localStorage.getItem('Token'),
            "DeveloperID": devId,
            "ContextID": localStorage.getItem('ContextID')
        },
        success: function(data, status) {
            //console.log(data);
            app.preloader.hide();
            if(data.Result.CountOfSuccessful > 0){
            //if(data.ListOfSuccess){
                console.log('label created, refresh');
                var refresh = true;
                if(despatched) {
                    markAsDespatched(refresh);
                } else {
                    searchForOrder(orderNr);
                }
                return;
            } 
            toast(data.Result.ErrorList[0].ErrorMessage);
        },
        error: function(xhr, textStatus, errorThrown) {
            app.preloader.hide();
            console.log(xhr);
            console.log(textStatus);
            toast("Create label: "+errorThrown);
            //app.dialog.alert('Error.');
        }
    });
}

var labelCount;
function getLabelId(shipmentID, UniqueRef){//(UniqueRef, id, OrderNumber){
    var printer = localStorage.getItem('defaultPrinter');
    var ip = localStorage.getItem('defaultIP');
    if(printer == 'null'){
        app.dialog.confirm('Please select a default printer in Settings.', function () {
            app.views.main.router.navigate('/settings/');
        });
        return;
    }
    if(ip == 'null'){
        app.dialog.confirm('Please add printer IP in Settings.', function () {
            app.views.main.router.navigate('/settings/');
        });
        return;
    }
    var id = $('#order').attr('data-id');
    var OrderNumber = $('#order').attr('data-ord');
    var btn = $('.label-button button.color-green[data-id="'+shipmentID+'"]');
    $('#label-popup .page-content .block').html('');
    $('#label-popup .navbar .left').addClass('hide');
    
    var dialog = app.dialog.create({
        title: '<small class="text-color-blue">'+btn.text()+'</small><br>Print Label',
        animate: false,
        content: '',
        on: {
            open: function (e) {   
                $(e.$el).find('.dialog-title').css({'text-align':'center'});
                $(e.$el).find('.dialog-buttons').css({'justify-content':'center', 'padding-bottom':"24px", 'height':"auto"});
                $(e.$el).find('.dialog-buttons span').addClass('button color-green button-fill').css({'color':'#fff', 'margin':'0 10px','min-width':'auto','padding':'0 20px'});
                $(e.$el).find('.dialog-buttons span:first-child').removeClass('color-green').addClass('color-red');
            }
        },
        buttons: [{
            text:'Cancel',
            close:true
        }, {
            text:'Print',
            onClick: function(dialog, e){
                app.preloader.show();
                $.ajax({
                    type: 'POST',
                    url: api + 'print/getLabels',//?orderId='+id,
                    crossDomain: true,
                    contentType: "application/x-www-form-urlencoded",
                    dataType: "json",
                    processData: true,
                    headers: {
                        "ApiToken": "nvt.api."+localStorage.getItem('Token'),
                        "DeveloperID": devId,
                        "ContextID": localStorage.getItem('ContextID')
                    },
                    data: {
                        ApplicationID: appId,
                        OrderIDs:[id]
                    },
                    success: function(data) {
                    	//console.log('print/getLabels - order id '+id);
                        //console.log(data);
						if(data.length){
							$.get(data, function(content) {
								if(content == '') {
                                    app.preloader.hide();
                                    toast('Something is wrong with the label. Please try again.');
                                    return;
                                }
								var labels = $(content).map(function(){
									if(this.tagName !== 'BR') {
                                        return this.src.replace('data:image/png;base64,','');
                                    }
								}).get();
								console.log(labels);
								$.each(labels, function(i,base64Img){
                                    console.log('createImage for ordrer '+OrderNumber+' - '+shipmentID);
                                    if(!app.device.desktop) {
                                        createImage(base64Img, OrderNumber+'i'); //OrderNumber
                                    }
								});
                            }).done(function() {
                                markAsDespatched(true);
							});
                        } else {
                            toast('No label.');
                        }
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        toast(errorThrown);
                    }
                });
            }
        }]
    }).open();
}

function getOrders(){
    $.ajax({
        type: 'GET',
        url: api + 'orders/GetOrdersStatistic',
        crossDomain: true,
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        processData: true,
        data: {
            context: localStorage.getItem('ContextID'),
            status: 'Despatched'
        },
        headers: {
            "ApiToken": "nvt.api."+localStorage.getItem('Token'),
            "DeveloperID": devId,
            "ContextID": localStorage.getItem('ContextID')
        },
        success: function(data) {
            $('.todayOrders').text(data.despatchedOrdersExceptToday+'/'+data.ordersToDespatchExceptToday);
        },
        error: function(error) {
            console.log(error);
        }   
    });
}

/*
function getOrders(modifiedFrom, skip) {
    //var start_time = new Date().getTime();
    $.ajax({
        type: 'GET',
        url: api + 'orders',
        crossDomain: true,
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        processData: true,
        data: {
            modifiedFrom: modifiedFrom,
            pageSize: 5000,
            skip: skip //optional, default = 0
        },
        headers: {
            "ApiToken": "nvt.api."+localStorage.getItem('Token'),
            "DeveloperID": devId,
            "ContextID": localStorage.getItem('ContextID')
        },
        success: function(data, status) {
            //console.log(data);
            $('.todayOrders').text(data.length);
            //var x = new Date().getTime() - start_time;
            //console.log(x);
        },
        error: function(xhr, textStatus, errorThrown) {
            if(xhr.status == 403){
                var curPage = app.views.main.router.currentPageEl;
                if($(curPage).data('name') !== 'lockscreen'){
                    toast('Previous user has been logged out, please log in with your PIN.');
                    app.views.main.router.navigate('/lockscreen/');
                }
                return;
            }
            app.dialog.alert(errorThrown);
        }
    });  
}
*/

function getProfile(){
    $.ajax({
        type: 'GET',
        url: api + 'company',
        cache: false,
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        headers: {
            "ApiToken": "nvt.api."+localStorage.getItem('Token'),
            "DeveloperID": devId,
            "ContextID": localStorage.getItem('ContextID')
        },
        success: function(data, status) {
            //console.log(data);
            if(data){
                $('.profile-email').val(data.EmailAddress);
                $('.profile-company-name').val(data.CompanyName);
                $('.profile-phone').val(data.TelephoneNumber);
                $('.profile-registration-number').val(data.RegistrationNumber);
                $('.profile-address-line1').val(data.AddressLine1);
                $('.profile-address-line2').val(data.AddressLine2);
                $('.profile-address-line3').val(data.AddressLine3);
                $('.profile-country').val(data.Country);
                $('.profile-county').val(data.County);
                $('.profile-postcode').val(data.Postcode);
                $('.profile-website-url').val(data.WebsiteURL);
                $('.profile-vat-number').val(data.VATNumber);
            }
        },
        error: function(xhr, textStatus, errorThrown) {
            console.log(xhr)
            console.log(errorThrown)
            console.log(textStatus)
            toast(errorThrown);
            //app.dialog.alert('Error.');
        }
    });
}

function getPlatforms() {
    var platforms = []
    $.ajax({
        type: 'GET',
        url: api + 'platforms',
        cache: false,
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        headers: {
            "ApiToken": "nvt.api."+localStorage.getItem('Token'),
            "DeveloperID": devId,
            "ContextID": localStorage.getItem('ContextID')
        },
        success: function(data, status) {
            //console.log('get platforms')
            //console.log(data);
            if(data.length){
                $.each(data, function(i,v){
                    //console.log(v);
                    platforms.push({
                        Type: v.Type,
                        ID: v.ID,
                        Name: v.Name,
                        IsArchived: v.IsArchived
                    });
                });
                localStorage.setItem('platforms', JSON.stringify(platforms));
            }
        },
        error: function(xhr, textStatus, errorThrown) {
            console.log("Get account platforms: "+errorThrown);
            //app.dialog.alert('Error.');
        }
    });
}

/**
 * Convert a base64 string in a Blob according to the data and contentType.
 * 
 * @param b64Data {String} Pure base64 string without contentType
 * @param contentType {String} the content type of the file i.e (image/jpeg - image/png - text/plain)
 * @param sliceSize {Int} SliceSize to process the byteCharacters
 * @see http://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
 * @return Blob
 */
//https://ourcodeworld.com/articles/read/150/how-to-create-an-image-file-from-a-base64-string-on-the-device-with-cordova
function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;
    var byteCharacters = atob(b64Data);
    var byteArrays = [];
    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);
        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }
        var byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
    }
    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
}

/**
 * Create a Image file according to its database64 content only.
 * 
 * @param folderpath {String} The folder where the file will be created
 * @param filename {String} The name of the file that will be created
 * @param content {Base64 String} Important : The content can't contain the following string (data:image/png[or any other format];base64,). Only the base64 string is expected.
 */
function savebase64AsImageFile(folderpath, filename, content, contentType) {
    // Convert the base64 string in a Blob
    var DataBlob = b64toBlob(content, contentType);
    //console.log("Starting to write the file");
    window.resolveLocalFileSystemURL(folderpath, function (dirEntry) {
        //console.log('file system open: ' + dirEntry.name);
        saveFile(dirEntry, DataBlob, filename+'.png');
    }, function(error){
        console.log(error);
    });
}

function readBinaryFile(fileEntry) {
    fileEntry.file(function (file) {
        var reader = new FileReader();
        reader.onloadend = function() {
            //console.log("Successful file write");
            //console.log(this.result);
            //var blob = new Blob([new Uint8Array(this.result)], {type: "image/png"});
            //displayImage(blob);
        };
        reader.readAsArrayBuffer(file);
    }, function(error){
        console.log(error);
    });
}

/*
function displayImage(blob) {
    // Displays image if result is a valid DOM string for an image.
    // var elem = document.getElementById('imageFile');
    // Note: Use window.URL.revokeObjectURL when finished with image.
    var img = window.URL.createObjectURL(blob);
    $('#label-popup .page-content .block').html('<img id="blobLabel" src="' + img + '" />');
}
*/

function writeFile(fileEntry, dataObj, fileName) {
    // Create a FileWriter object for our FileEntry (log.txt).
    fileEntry.createWriter(function (fileWriter) {
        fileWriter.onwriteend = function() {
            //console.log("Successful file write...");
            //readFile(fileEntry);
            //readBinaryFile(fileEntry);
            //toast('Printing...');
            setTimeout(function(){
                //console.log('printLabel '+fileName);
                printLabel(fileName);
            }, 1000);
            /*
            var counter = 0;
            var i = setInterval(function(){
                console.log('printLabel')
                printLabel();
                counter++;
                if(counter === labelCount.getValue()) {
                    clearInterval(i);
                    markAsDespatched();
                    app.preloader.hide();
                }
            }, 100);
            */
        };
        fileWriter.onerror = function (e) {
            console.log("Failed file write: " + e.toString());
        };
        // If data object is not passed in, create a new Blob instead.
        if (!dataObj) {
            //console.log('nothing here');
            dataObj = new Blob(['some file data'], { type: 'text/plain' });
        }
        fileWriter.write(dataObj);
    });
}

function saveFile(dirEntry, fileData, fileName) {
    dirEntry.getFile(fileName, { create: true, exclusive: false }, function (fileEntry) {
        //console.log(fileEntry.nativeURL);
        //localStorage.setItem('labelNativeURL', fileName);
        writeFile(fileEntry, fileData, fileName);
    }, function(error){
        console.log(error);
    });
}

function createImage(myBase64, filename){
    //console.log('create image with name '+filename);
    // If your base64 string contains "data:image/png;base64,"" at the beginning, keep reading.
    //var myBase64 = "";
    // To define the type of the Blob
    //var contentType = "image/png";
    // if cordova.file is not available use instead :
    // var folderpath = "file:///storage/emulated/0/";
    //var folderpath = cordova.file.cacheDirectory;
    //var filename = "0000001537.png";
    //var filename = orderId;
    //console.log(folderpath);
    savebase64AsImageFile(cordova.file.cacheDirectory, filename, myBase64, "image/png"); //, function(){
        //getFileEntry(folderpath+filename+'.png');
    //});
    //console.log(folderpath);
    //console.log(filename);
    //var img = folderpath+filename; //'file:///data/app/cache/0000001537.png';
}

function getShippingServiceDesription(sc){
    //console.log('sc - getShippingServiceDesription')
    //console.log(sc);
    var contextId = localStorage.getItem('ContextID');
	var text = "";
	// only for stores:
	// BuyaparcelTest1 / BuyAParcel - GoLive / Net Candles 
	if(contextId === "f59de33d-aca2-4b29-af35-916929166e15" || contextId === "d6a4e29c-308b-461c-a787-3e42c629930c" || contextId === "0fb757d6-753e-43ca-b211-7adacb466306"){
        if(sc.DeliveryServices[0].Name == 'Other'){
            text += sc.DeliveryServices[0].Method;
        } else {
            text += sc.DeliveryServices[0].Name;
        }
        /*
		if (sc.SelectedProviderType) {
			if(sc.SelectedProviderType !== "UKmail") {
				text += sc.SelectedProviderType;
			}
		}
		if (sc.Description){
		    if(sc.SelectedProviderType === "Hermes"){
			    var hermesDescription = sc.Description;
			    text += hermesDescription.replace("Courier","");
		    }
	 		if(sc.SelectedProviderType === "UKmail"){
	 			var desc = sc.Description;
	 			if(sc.Code === "20") {
	 				desc = "Bagit";
                }
                if(sc.DeliveryServices[0].Name == "DHL Ireland") {
                    text += sc.DeliveryServices[0].Name;
                } else {
                    text += " "+desc;
                }
			    //text += " "+desc;
		    }
		}
		if (sc.Code) {
			if(sc.SelectedProviderType === "UKmail"){
				var type = "";
				switch(sc.Code){
					case "1": 
						type = "Parcel";
						break;
					case "20": 
						type = "- Large";
						break;
					case "48":
						type = "Parcel";
						break;
					case "72":
						if(sc.Description === "48 Hour+") {
							type = "Parcel";
						} else {
							type = "Parcel w/ Signature";
						}
						break;
					default: 
						type = sc.Code;
                }
                if(sc.DeliveryServices[0].Name !== "DHL Ireland") {
                    text += " "+type;
                }
			}
        }
        */
	} else {
	    if (sc.SelectedProviderType) {
			text += sc.SelectedProviderType;
		}
		if (sc.Description || sc.ServiceDescription) {
			if (text.length > 0) {
				text += ' - ';
			}
			text += sc.Description || sc.ServiceDescription;
		}
	    if (sc.ServiceCode || sc.Code) {
			if (text.length > 0) {
				text += ' - ';
			}
			text += sc.ServiceCode || sc.Code;
		}
	}
	return text;
}

function getShipmentDescription(shipment){
	//console.log(shipment)
    if (shipment && shipment.Attributes) {
        var text = '';
        var netDepatchTarifCode = '';
        if (shipment.Attributes.ProviderType) {
            text = text + shipment.Attributes.ProviderType;
        }
        if (shipment.Attributes.TariffCode) {
            netDepatchTarifCode = shipment.Attributes.TariffCode;
        }
        if (shipment.Attributes.Code) {
            text = text + " - " + shipment.Attributes.Code;
        }
        if (netDepatchTarifCode && text) {
            text = text + " - " + netDepatchTarifCode;
        }
        return text;
    }
}

function searchForMapping(mappings, deliveryService, deliveryMethod){
	var selectedMapping = null;
	for(var j = 0; j < mappings.length; j++) {
		var mapping = mappings[j];
		if (mapping.DeliveryServices && mapping.DeliveryServices.length > 0){
			var anySuitable = mapping.DeliveryServices.some(function(ds){
				return ds.Name == deliveryService && ds.Method == deliveryMethod;
            });
			if (anySuitable){
				selectedMapping = mapping;
				break;
			}	 
		}
    }
	return selectedMapping;
}

function getRealShippingService(deliveryService, deliveryMethod) {
  	var contextId = localStorage.getItem('ContextID');
	var shipmentConfigKey = "ShipmentConfig-"+contextId;
    var data = localStorage.getItem(shipmentConfigKey);
	if (data == null)
		return null;
		//throw error that shipmentConfig is not downloaded
	var shippingConfig = JSON.parse(data);
	var theRealShippingService = null;
	if (shippingConfig.RoyalMailIntegrations && shippingConfig.RoyalMailIntegrations.length > 0){
		for(var i = 0; i < shippingConfig.RoyalMailIntegrations.length; i++) {
			var integration = shippingConfig.RoyalMailIntegrations[i];
			if(integration.Mappings && integration.Mappings.length > 0){
                theRealShippingService = searchForMapping(integration.Mappings,deliveryService,deliveryMethod);
            }
			if (theRealShippingService){
                theRealShippingService.AppDefaultShipment = deliveryService;
				theRealShippingService.SelectedProviderType = "Royal Mail";
				break;
			}
		};
		if (theRealShippingService)
			return theRealShippingService;
	}
	if (shippingConfig.UkMailIntegrations && shippingConfig.UkMailIntegrations.length > 0){
		for(var i = 0; i < shippingConfig.UkMailIntegrations.length; i++) {
			var integration = shippingConfig.UkMailIntegrations[i];
			if(integration.Mappings && integration.Mappings.length > 0){
				theRealShippingService = searchForMapping(integration.Mappings,deliveryService,deliveryMethod);
			}
			if (theRealShippingService){
                theRealShippingService.AppDefaultShipment = deliveryService;
				theRealShippingService.SelectedProviderType = "UKmail";
				break;
			}
		};
		if (theRealShippingService)
			return theRealShippingService;
	}
	if (shippingConfig.SmartConsignIntegrations && shippingConfig.SmartConsignIntegrations.length > 0){
		for(var i = 0; i < shippingConfig.SmartConsignIntegrations.length; i++) {
			var integration = shippingConfig.SmartConsignIntegrations[i];
			if(integration.Mappings && integration.Mappings.length > 0){
				theRealShippingService = searchForMapping(integration.Mappings,deliveryService,deliveryMethod);
			}
			if (theRealShippingService){
                theRealShippingService.AppDefaultShipment = deliveryService;
				theRealShippingService.SelectedProviderType = "SmartConsign";
				break;
			}
		};
		if (theRealShippingService)
			return theRealShippingService;
	}
	if (shippingConfig.MyHermesIntegrations && shippingConfig.MyHermesIntegrations.length > 0){
		for(var i = 0; i < shippingConfig.MyHermesIntegrations.length; i++) {
			var integration = shippingConfig.MyHermesIntegrations[i];
			if(integration.Mappings && integration.Mappings.length > 0){
				theRealShippingService = searchForMapping(integration.Mappings,deliveryService,deliveryMethod);
			}
			if (theRealShippingService){
                theRealShippingService.AppDefaultShipment = deliveryService;
				theRealShippingService.SelectedProviderType = "MyHermes";
				break;
			}
		};
		if (theRealShippingService)
			return theRealShippingService;
	}
	if (shippingConfig.MyHermesSandboxIntegrations && shippingConfig.MyHermesSandboxIntegrations.length > 0){
		for(var i = 0; i < shippingConfig.MyHermesSandboxIntegrations.length; i++) {
			var integration = shippingConfig.MyHermesSandboxIntegrations[i];
			if(integration.Mappings && integration.Mappings.length > 0){
				theRealShippingService = searchForMapping(integration.Mappings,deliveryService,deliveryMethod);
			}
			if (theRealShippingService){
                theRealShippingService.AppDefaultShipment = deliveryService;
				theRealShippingService.SelectedProviderType = "MyHermesSandbox";
				break;
			}
		};
		if (theRealShippingService)
			return theRealShippingService;
	}
	if (shippingConfig.DPDIntegrations && shippingConfig.DPDIntegrations.length > 0){
		for(var i = 0; i < shippingConfig.DPDIntegrations.length; i++) {
			var integration = shippingConfig.DPDIntegrations[i];
			if(integration.Mappings && integration.Mappings.length > 0){
				theRealShippingService = searchForMapping(integration.Mappings,deliveryService,deliveryMethod);
			}
			if (theRealShippingService){
                theRealShippingService.AppDefaultShipment = deliveryService;
				theRealShippingService.SelectedProviderType = "DPD";
				break;
			}
		};
		if (theRealShippingService)
			return theRealShippingService;
	}
	if (shippingConfig.ParcelForceIntegrations && shippingConfig.ParcelForceIntegrations.length > 0){
		for(var i = 0; i < shippingConfig.ParcelForceIntegrations.length; i++) {
			var integration = shippingConfig.ParcelForceIntegrations[i];
			if(integration.Mappings && integration.Mappings.length > 0){
				theRealShippingService = searchForMapping(integration.Mappings,deliveryService,deliveryMethod);
			}
			if (theRealShippingService){
                theRealShippingService.AppDefaultShipment = deliveryService;
				theRealShippingService.SelectedProviderType = "ParcelForce";
				break;
			}
		};
		if (theRealShippingService)
			return theRealShippingService;
	}
	if (shippingConfig.ParcelHubIntegrations && shippingConfig.ParcelHubIntegrations.length>0){
		for(var i = 0; i < shippingConfig.ParcelHubIntegrations.length; i++) {
			var integration = shippingConfig.ParcelHubIntegrations[i];
			if(integration.Mappings && integration.Mappings.length>0){
				theRealShippingService = searchForMapping(integration.Mappings,deliveryService,deliveryMethod);
			}
			if (theRealShippingService){
                theRealShippingService.AppDefaultShipment = deliveryService;
				theRealShippingService.SelectedProviderType = "ParcelHub";
				break;
			}
		};
		if (theRealShippingService)
			return theRealShippingService;
	}
	if (shippingConfig.HermesRoutingIntegrations && shippingConfig.HermesRoutingIntegrations.length>0){
		for(var i = 0; i < shippingConfig.HermesRoutingIntegrations.length; i++) {
			var integration = shippingConfig.HermesRoutingIntegrations[i];
			if(integration.Mappings && integration.Mappings.length>0){
				theRealShippingService = searchForMapping(integration.Mappings,deliveryService,deliveryMethod);
			}
			if (theRealShippingService){
                theRealShippingService.AppDefaultShipment = deliveryService;
				theRealShippingService.SelectedProviderType = "Hermes"; //"HermesRouting";
				break;
			}
		};
		if (theRealShippingService)
			return theRealShippingService;
	}
	if(!theRealShippingService)
		//$"This shipping method in order <a href='#/order/{orderID}'>{orderNumber}</a> is not mapped to any courier shipping option"
		return theRealShippingService;	
}